var $first=true;
var nowPage=1;
$(document).ready(function () {

    /**全局变量*/
    _ACTIVE_ = "active";

    //收藏批量管理事件
    $(".J-fun-btn").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
            $(this).html("取消批量管理");
            $(".J-collection-list").addClass(_ACTIVE_).removeClass("hover-fun");
        }else{
            $(this).removeClass(_ACTIVE_);
            $(this).html("批量管理");
            $(".J-collection-list").removeClass(_ACTIVE_).addClass("hover-fun");
            $(".J-fun-all").removeClass(_ACTIVE_);
            $(".J-collection-ul li").removeClass("allDel");
        }
    });
    $(".J-fun-all").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
            $(".J-collection-ul li").addClass("allDel");
        }else{
            $(this).removeClass(_ACTIVE_);
            $(".J-collection-ul li").removeClass("allDel");
        }
    });
    $(".J-collection-ul").on("click","a",function(){
        if(!$(".J-collection-list").hasClass(_ACTIVE_)){
            return true;
        }else{
            $(this).parent("li").toggleClass("allDel");
            $(".J-collection-ul").each(function(){
                var $_len = $(this).find("li").length;
                var $_len_xz = $(this).find("li.allDel").length;
                if($_len > $_len_xz){
                    $(".J-fun-all").removeClass(_ACTIVE_);
                }else if($_len === $_len_xz){
                    $(".J-fun-all").addClass(_ACTIVE_);
                }
            })
            return false;
        }
    })
    $(".J-fun-del").click(function(){
        $(".J-collection-ul").each(function(){
            var $_num = $(this).find("li.allDel").length;
            if($_num > 0){
                $(".J-funPopup").fadeIn(100);
            }
        })
        // showCollection();
    })
    $(".J-collection-list").on("click",".J-delBtn",function(){
                $(".J-funPopup").fadeIn(100);
        $(this).parents("li").addClass("remove");
        $(this).parents("li").siblings().removeClass("remove");
        // showCollection();
    })
    $(".J-funPopup .J-closeFunPopup").click(function(){
        $(".funPopup").fadeOut(100);
    })
    $(".J-funPopup .J-confirmDel").click(function(){
        $(".J-funPopup").fadeOut(100);
        $(".J-collection-ul").find("li.remove").remove();
        $(".J-collection-ul").find("li.allDel").remove();
        $(".J-collection-list").find(".J-fun-all").removeClass("active");
        // showCollection();
        setTimeout(function(){
            load(nowPage);
        },500);
    })

    load(nowPage);
});
function scrollTo(element,speed) {
	if(!speed)speed = 300;
	if(!element) $("html,body").animate({scrollTop:0},speed);
	else{
		if(element.length>0){
			$("html,body").animate({scrollTop:$(element).offset().top-140},speed);
		}
	}
}
function load(page) {
	var $url = getUrl(page);
	$('#loading').show();
	if(!$first)scrollTo('#loading',500);
	$first=false;
	$('#list').each(function () {
		var $list = this;
        setTimeout(function () {
            $($list).html('');
        },260);
		var $listCode=[];
		setTimeout(function () {
			$.get($url, function (data) {
                if(data.total<=0){
                    //showCollection();
                    $('#loading').hide();
                    // $(".J-fun-btn").hide();
                    // $(".J-fun-all").hide();
                    $(".J-fun-del").hide();
                    $(".collection-fun").hide();
                    $(".J-collection-ul-none").show();
                    $(".J-page-number").hide();
                    return;
                }else{
                    $(".J-fun-btn").show();
                    $(".collection-fun").show();
                }
                $(".J-page-number").show();
				$('#loading').hide();
				if(data.now<1){
					$('#list-none').show();
				}else{
					$($list).show();
				}
				for (var i = 0; i < data.rows.length; i++) {
					var row = data.rows[i];
					$listCode.push(
						'<li>'+
						'<a href="'+row.href+'"><img src="'+row.src+'"><span class="collection-name">'+row.name+'</span><span class="collection-model">'+row.model+'</span>'+
						'<div class="delBtn J-delBtn"></div></a></li>');
				}
				$($list).html($listCode.join(''));
				var nav = [];
				var navUl = $('#list-page');
				//$(c).append(navContainer);
				//$(navContainer).append(navUl);
				nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
				var num=5;
				if(data.total>num) {

					if(data.total<5){
						num = data.total;
					}
					if (data.now < num) {
						for (var i = 1; i <= num; i++) {
							var str = '0' + i;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');

						}
						if(data.total>num) nav.push('<li>...</li>');
					} else if (data.now > data.total - num) {
						nav.push('<li>...</li>');
						for (var i = data.total - num; i <= data.total; i++) {
							var str = i + '';
							if (str.length <= 1) str = '0' + str;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
						}
					} else {
						nav.push('<li>...</li>');
						for (var i = data.now - 1; i < data.now + 4; i++) {
							var str = i + '';
							if (str.length <= 1) str = '0' + str;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
						}
						nav.push('<li>...</li>');
					}
					$(navUl).html(nav.join(''));
					if(data.now+4 < data.total) navUl.append('<li><a href="javascript:load('+(data.total)+');">'+(data.total)+'</a></li>');
                    navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
                    nowPage = data.now;
				}
			});
		},2000);
	});
}
function getUrl(page){
    //page='';
    if(page<2)
	return "static/json/data-list-collection.json";
    else return "static/json/blank.json";
}