var $first=true;
$(document).ready(function () {

	/*点击兑换判断是否登录*/
	$(".m-club-dh-list").on("click",".J-redeemNow",function(){
		var $href = $(this).attr("data-href");
		if($(".userToggle").is(":hidden")){
            $(this).attr("href","login.html");
		}else{
            $(this).attr("href",$href);
		}
	});


	load(1);
});
function scrollTo(element,speed) {
	if(!speed)speed = 300;
	if(!element) $("html,body").animate({scrollTop:0},speed);
	else{
		if(element.length>0){
			$("html,body").animate({scrollTop:$(element).offset().top-140},speed);
		}
	}
}
function load(page) {
	var $url = getUrl(page);
	$('#loading').show();
	if(!$first)scrollTo('#loading',500);
	$first=false;
	$('#J-list').each(function () {
		var $list = this;
		console.log(this);
		$($list).html('');
		var $listCode=[];
		setTimeout(function () {
			$.get($url, function (data) {
				$('#loading').hide();
				if(data.now<1){
					$('#list-none').show();
				}else{
					$($list).show();
				}
				for (var i = 0; i < data.rows.length; i++) {
					var row = data.rows[i];
					$listCode.push(
						'<div class="club-dh-item">'+
						'<div class="dh-img">' +
						'<img src="'+row.src+'">' +
						'<div class="dh-QR"><p><img src="'+row.qrImg+'" alt=""><span>扫一扫，查看礼品详情</span></p></div>'+
						'</div>'+
						'<div class="dh-txt">'+
						'<div class="dh-pro-tit">'+row.tit+'</div><span class="integral">'+row.integral+'</span><a class="J-redeemNow" href="javascript:;" data-href="'+row.href+'">立即兑换</a><a class="J-qr" href="javascript:;" data-href="'+row.link+'">查看详情</a>'+
						'</div>'+
						'</div>');
				}
				$($list).html($listCode.join(''));
			});
		},2000);
	});
}
function getUrl(page){
	page='';
	return "static/json/data-list-huiyuan.json";
}