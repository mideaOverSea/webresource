/**cookies提示*/
;(function () {
    var htmlString = '<div class="cookies tips"><div class="cm-con"><div class="content"><div class="text"><p class="text-p"><img src="//source.midea.com/colmoweb/images/v2/cookies.png">本网站会使用Cookies以提升您的访问体验。如继续浏览本网站，表示您同意我们使用Cookies。相关政策请见本网站的隐私政策。</p></div><div class="btn line middle">确定</div></div></div><div class="close iconfont icon-cha"><img src="//source.midea.com/colmoweb/images/v2/closeAi.png"></div></div>';
    $('body').append(htmlString);
    var cookies = $('body .cookies');
    function setcookie() {
        var d = new Date();
        var res = null;
        d.setTime(d.getTime() + 8760 * 60 * 60 * 1000);
        document.cookie = "COLMO=already;expires=" + d.toGMTString()+"domain="+window.location.originUrl+";path=/";
        res = document.cookie;
        return res;
    }
    function getCookie(name) {
        var arr,
            reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        if (arr = document.cookie.match(reg)) {
            return unescape(arr[2]);
        } else {
            return null;
        }
    }
    cookies.find('.close').on('click', function () {
        cookies.stop().fadeOut(500);
    });
    /*判断网页是否是第一次浏览，如果第一次则Cookies，然后设置cookie值，否则把隐藏*/
    // console.log('cokkie=' + getCookie('COLMO'))
    if (getCookie('COLMO') === null) {
        cookies.stop().fadeIn(0);
    } else {
        cookies.stop().fadeOut(0);
    }
    cookies.find('.btn').on('click', function () {
        setcookie();
        cookies.stop().fadeOut(500);
    });
})();

/**cookies提示  End*/