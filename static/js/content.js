/**滚动条*/
(function($){
    $(window).load(function(){
        var mCustomScrollbar = $("#J-sc-list,#J-city-list,.sc-cont,#J-pro-list,.selector-list,.logistics-popup ul").mCustomScrollbar({
            scrollInertia:150,
            autoDraggerLength: true,
            autoExpandScrollbar:true,
            alwaysShowScrollbar:2,
            mouseWheel:{
                enable:true,
                scrollAmount:10,
                preventDefault:true,
                invert:true,
            },
            advanced:{
                updateOnBrowserResize:Boolean,
                updateOnContentResize:Boolean,
                autoExpandHorizontalScroll:Boolean,
                autoScrollOnFocus:Boolean
            }
        });
    })
})(jQuery);

$(document).ready(function(){

    document.documentElement.style.setProperty('--theme-color','#ae6437');

    /**公用组件*/

    /*全局变量，元素激活状态*/
    _ACTIVE_ = "active";

    /*判断浏览器版本*/
    var $userAgent = navigator.userAgent.toLowerCase();
    $.browser = {
        version: ($userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        safari: /webkit/.test($userAgent),
        opera: /opera/.test($userAgent),
        msie: /msie/.test($userAgent) && !/opera/.test($userAgent),
        mozilla:/mozilla/.test($userAgent)&&!/(compatible|webkit)/.test($userAgent)
    };

    /*屏幕滚动动画*/
    var wow = new WOW({
        boxClass: 'wow',
        animateClass:'animated',
        offset: 0,
        mobile: true,
        live: true,
        callback: function(box) {
            // console.log(box);
        }
    });
    wow.init();

    /*判断当前分辨率，更换pc、mobile的banner图*/
    var srcChange = function () {
        var cWidth = document.documentElement.clientWidth;
        if (cWidth > 768) {
            $('*[data-src-pc]').each(function () {
                $(this).attr('src', $(this).attr('data-src-pc'));
            })
        } else {
            $('*[data-src-mobile]').each(function () {
                $(this).attr('src', $(this).attr('data-src-mobile'));
            })
        }
    }
    srcChange();

    /*返回顶部*/
    $(".J-goTop").click(
        function(){$('html,body').animate({scrollTop:'0px'},200);
    });

    /*移动端虚拟键盘弹出样式修正*/
    var hrt = document.documentElement.clientHeight;
    window.onload = function() {
        if($('J-loginWrap').length > 0){
            document.getElementById('J-loginWrap').style.height = hrt + 'px';
        }
    };

    var wHeight = window.innerHeight;
    window.addEventListener('resize', function(){
        var hh = window.innerHeight;
        if(wHeight > hh){
            $(document).find("#J-loginFooter").hide();
        }else{
            $(document).find("#J-loginFooter").show();
        }
        wHeight = hh;
    });

    /**公用组件  end*/

    /**场景页*/
    function sceneSwitchA(){
        $(".sceneCon-link .swiper-slide").each(function(){
            var _a = $(this).find("a");
            var _bgImg = _a.attr("data-img");
            var _hbgImg = _a.attr("data-hover-img");
            _a.css('background-image','url('+_bgImg+')');
            _a.hover(function(){
                _a.css('background-image','url('+_hbgImg+')');
            },function(){
                _a.css('background-image','url('+_bgImg+')');
            });
            if(!$(this).hasClass(_ACTIVE_)){
                _a.css('background-image','url('+_bgImg+')');
            }else{
                _a.css('background-image','url('+_hbgImg+')');
                _a.hover(function(){
                    _a.css('background-image','url('+_hbgImg+')');
                });
            }
        });
    }
    sceneSwitchA();
    function sceneSwitchB(){
        $(".sceneCon-txt li").each(function(){
            var _a = $(this).find("a");
            var _bgImg = _a.attr("data-img");
            var _hbgImg = _a.attr("data-hover-img");
            _a.css('background-image','url('+_bgImg+')');
            _a.hover(function(){
                _a.css('background-image','url('+_hbgImg+')');
            },function(){
                _a.css('background-image','url('+_bgImg+')');
            });
            if(!$(this).hasClass(_ACTIVE_)){
                _a.css('background-image','url('+_bgImg+')');
            }else{
                _a.css('background-image','url('+_hbgImg+')');
                _a.hover(function(){
                    _a.css('background-image','url('+_hbgImg+')');
                });
            }
        });
    }
    sceneSwitchB();
    function sceneBanner(){
        $(".m-sub-banner .swiper-container").each(function(){
            var $len = $(this).find(".swiper-slide").length;
            if($len > 1){
                var swiper = new Swiper('.m-sub-banner .swiper-container', {
                    nextButton: '.m-sub-banner .swiper-button-next',
                    prevButton: '.m-sub-banner .swiper-button-prev',
                    pagination: '.m-sub-banner .swiper-pagination',
                    calculateHeight : true,
                    autoHeight: true,
                    autoplay:5000,
                    paginationClickable :true,
                    autoplayDisableOnInteraction : false,
                    paginationBulletRender: function (swiper, index, className) {
                        return '<span class="' + className + '">0' + (index + 1) + '</span>';
                    },
                    onSwiperCreated: function(mySwiper){
                        if($.browser.msie&&($.browser.version === "9.0")){
                            var $_pageLen = $(".J-index-banner .pagination span");
                            $_pageLen.each(function(){
                                var $_index = $(this).index() + 1;
                                $(this).html('0'+$_index);
                            })
                        }
                    }
                });
            }else{
                var swiper = new Swiper('.m-sub-banner .swiper-container', {
                    nextButton: '.m-sub-banner .swiper-button-next',
                    prevButton: '.m-sub-banner .swiper-button-prev',
                    pagination: '.m-sub-banner .swiper-pagination',
                    calculateHeight : true,
                    autoHeight: true,
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                });
            }
        })
    }
    if($(".m-sub-banner .swiper-container").length > 0){
        sceneBanner();
    };
    function sceneSwiper(){
        var _len = $(".sceneCon-Swiper .swiper-slide").length;
        if(_len > 1){
        var swiper = new Swiper('.sceneCon-Swiper .swiper-container', {
            nextButton: '.sceneCon-Swiper .swiper-button-next',
            prevButton: '.sceneCon-Swiper .swiper-button-prev',
            calculateHeight : true,
            autoHeight: true,
            spaceBetween: 10,
            loop:true,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
        });
        }else{
            var swiper = new Swiper('.sceneCon-Swiper .swiper-container', {
                nextButton: '.sceneCon-Swiper .swiper-button-next',
                prevButton: '.sceneCon-Swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                loop:false,
                // autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
        }
    }
    if($(".sceneCon-Swiper").length > 0){
        sceneSwiper()
    };
    function scenelinkSwiper(){
        if($(window).width() > 768){
            var swiper = new Swiper('.sceneCon-link .swiper-container', {
                nextButton: '.sceneCon-link .swiper-button-next',
                prevButton: '.sceneCon-link .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                slidesPerView:4,
                loop:false,
                noSwiping : true,
                // autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
        }else{
            var swiper = new Swiper('.sceneCon-link .swiper-container', {
                nextButton: '.sceneCon-link .swiper-button-next',
                prevButton: '.sceneCon-link .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                slidesPerView:2,
                loop:false,
                noSwiping : true,
                // autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
        }

    }
    if($(".sceneCon-link").length > 0){
        scenelinkSwiper()
    };
    $(".sceneCon-link .swiper-slide").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
            $(".sceneCon-txt .sceneCon-txt-item").eq($(this).index()).show().siblings().hide();
        }else{
            $(this).removeClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
            $(".sceneCon-txt .sceneCon-txt-item").eq($(this).index()).hide();
        }
        // $(this).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
        // $(".sceneCon-txt .sceneCon-txt-item").eq($(this).index()).show().siblings().hide();
        sceneSwitchA()
    });
    // $('.sceneCon-txt').mouseleave(function(){
    //     $('.sceneCon-txt .sceneCon-txt-item').hide();
    //     //console.log($('.sceneCon-link li'));
    //     $('.sceneCon-link .swiper-slide').removeClass('active');
    //     $('.sceneCon-link .swiper-slide>a').each(function(){
    //         var url = this.style.backgroundImage;
    //
    //         url=url.replace('-on','');
    //         console.log(url);
    //         this.style.backgroundImage=url;
    //     });
    // });

    /**首页*/

    /*首页下滑查看更多*/
    $(".J-index-more").click(function(){
        $('html,body').animate({scrollTop: ($(".section2-tit").offset().top +1)},200);
    })

    /*首页banner下拉快速入口*/
    $(".J-upBtn").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
            $(".J-up-con").addClass("anim").slideDown(200);
        }else{
            $(this).removeClass(_ACTIVE_);
            $(".J-up-con").removeClass("anim").slideUp(200);
        }
    });

    /*首页轮播*/
    function indexBanner(){
        var mySwiper = new Swiper ('.J-index-banner .swiper-container', {
            pagination: '.J-index-banner .pagination',
            paginationClickable :true,
            calculateHeight : true,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : false,
            paginationBulletRender: function (swiper, index, className) {
                return '<span class="' + className + '">0' + (index + 1) + '</span>';
            },
            onSwiperCreated: function(mySwiper){
                if($.browser.msie&&($.browser.version === "9.0")){
                    var $_pageLen = $(".J-index-banner .pagination span");
                    $_pageLen.each(function(){
                        var $_index = $(this).index() + 1;
                        $(this).html('0'+$_index);
                    })
                }
            }
        })
        $(".J-index-banner .swiper-slide").each(function(){
            if($(this).find("video").length > 0){
                $(this).find(".J-bannerBtn").show();
            }else{
                $(this).find(".J-bannerBtn").hide();
            }
        })
    }
    indexBanner();

    /*首页AI全图景*/
    function indexAI(){
        $(".J-index-ai-swiper .swiper-slide").click(function(){
            $(this).find(".J-index-ai-list").addClass(_ACTIVE_).fadeIn(200);
            $(".J-index-ai .J-closeAi").fadeIn(200);
            $(".J-index-ai-list .J-index-ai-item").eq($(this).index()).addClass(_ACTIVE_);
        });

        $(".J-index-ai .J-closeAi").click(function(){
            $(this).fadeOut(100);
            $(".J-index-ai-list").removeClass(_ACTIVE_).fadeOut(200);
            $(".J-index-ai-list .J-index-ai-item").removeClass(_ACTIVE_);
            $(".J-ai-point").fadeIn(200);
            $(".J-ai-point-popup").removeClass(_ACTIVE_).fadeOut(200);
            $(".J-ai-point-popup .J-ai-popup-item").removeClass(_ACTIVE_);
            event.stopPropagation();
        });
        $(".J-index-ai-item").each(function(){
            var $this = $(this);
            var $btn = $this.find(".J-ai-point");
            var $win = $btn.siblings(".J-ai-point-popup");
            $btn.find(".J-point-btn").click(function(event){
                var $val = $(this).attr("data-val");
                $btn.fadeOut(200);
                $(".J-ai-point-popup").addClass(_ACTIVE_).fadeIn(200);
                $win.find(".J-ai-popup-item[data-val="+$val+"]").addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);

                event.stopPropagation();
            })
        });
        $(".J-ai-point-popup .J-ai-closePopup").click(function(){
            $(".J-ai-point").fadeIn(200);
            $(".J-ai-point-popup").fadeOut(200);
        });

        /*首页banner视频、首页全图景弹窗视频、探索colmo的banner视频播放*/
        var $url;
        $url= $(".J-playVideo,.J-banner-playBtn,.J-ts-video .J-playBtn").attr("data-url");
        $(".J-playVideo,.J-banner-playBtn,.J-ts-video .J-playBtn").click(function(){
            $(".J-index-video-popup").stop().fadeIn();
            $url=$(this).attr("data-url");
            playVideo($url);
        });

        $(".J-index-video-popup .J-closeVideo").click(function(){
            $(".J-index-video-popup").stop().fadeOut();
            $(".J-index-video-popup #video").empty();
        });

        function playVideo(url){
            jwplayer('video').setup({
                flashplayer: "../js/video/jwplayer.flash.swf",//调用播放器
                file: url,
                play:true,
                // image: src,
                width:'100%',
                height:'100%',
                events:{
                    onReady:function(){
                        this.play();
                    }
                }
            });
        }
        if($(".J-index-video-popup").length > 0){
            playVideo();
        }
    }
    indexAI();
    /*首页--AI全图景-弹框-产品功能*/
    $(".J-popup-item-txt ul").each(function(){
        var $_len = $(this).find("li").length;
        if($_len === 4){
            $(this).find("li").css("width","25%");
        }else if($_len === 5){
            $(this).find("li").css("width","20%");
        }
    })

    /*首页产品中心*/
    function indexPro(){
        var swiper = new Swiper ('.J-index-pro-swiper .swiper-container', {
            pagination: '.J-index-pro-swiper .pagination',
            paginationClickable :true,
            calculateHeight : true,
            autoplay:6000,
            autoplayDisableOnInteraction : false,
            loop : true,
            paginationBulletRender: function (swiper, index, className) {
                return '<span class="' + className + '">0' + (index + 1) + '</span>';
            },
            onSwiperCreated: function(mySwiper){
                if($.browser.msie&&($.browser.version === "9.0")){
                    var $_pageLen = $(".J-index-pro-swiper .pagination span");
                    $_pageLen.each(function(){
                        var $_index = $(this).index() + 1;
                        $(this).html('0'+$_index);
                    })
                }
            }
        })
    }
    indexPro()

    /*首页新闻推荐*/
    function indexNews(){
        var swiper = new Swiper ('.J-index-news-swiper .swiper-container', {
            pagination: '.J-index-news-swiper .pagination',
            paginationClickable :true,
            calculateHeight : true,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : true,
            paginationBulletRender: function (swiper, index, className) {
                return '<span class="' + className + '">0' + (index + 1) + '</span>';
            },
            onSwiperCreated: function(mySwiper){
                if($.browser.msie&&($.browser.version === "9.0")){
                    var $_pageLen = $(".J-index-news-swiper .pagination span");
                    $_pageLen.each(function(){
                        var $_index = $(this).index() + 1;
                        $(this).html('0'+$_index);
                    })
                }
            }
        })
    }
    indexNews();

    /*移动端-AI全图景*/
    function mobAI(){
        if($(window).width() > 1024){
            var swiper = new Swiper ('.J-index-ai-swiper .swiper-container', {
                pagination: '.J-index-ai-swiper .swiper-pagination',
                paginationClickable :true,
                slidesPerView:'4',
                calculateHeight : true,
                noSwiping : true,
                // autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : false,
            })
        }else if($(window).width() > 768){
            var swiper = new Swiper ('.J-index-ai-swiper .swiper-container', {
                pagination: '.J-index-ai-swiper .swiper-pagination',
                paginationClickable :true,
                slidesPerView:'2',
                spaceBetween : 20,
                calculateHeight : true,
                // noSwiping : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : false,
            })
            $(".J-index-ai-swiper .swiper-slide").removeClass("swiper-no-swiping");
        }else{
            var swiper = new Swiper ('.J-index-ai-swiper .swiper-container', {
                pagination: '.J-index-ai-swiper .swiper-pagination',
                paginationClickable :true,
                slidesPerView:'1',
                spaceBetween : 20,
                calculateHeight : true,
                noSwiping : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : false,
            })
            $(".J-index-ai-swiper .swiper-slide").removeClass("swiper-no-swiping");
        }
    }
    mobAI();

    /**首页   end*/


    /**购买-线下门店详情页*/
    /*详情页地图*/
    function subMap(){
        // 百度地图API功能
        var map = new BMap.Map("sub-map");
        var point = new BMap.Point(113.361243,22.915809);
        map.centerAndZoom(point, 18);
        var marker = new BMap.Marker(point);
        map.addOverlay(marker);
        marker.setAnimation(BMAP_ANIMATION_BOUNCE);
        map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    }

    /*详情页-其他门店*/
    function buySwiper(){
        if($(window).width() > 768){
            var swiper = new Swiper ('.J-buy-other-swiper .swiper-container', {
                prevButton:'.J-buy-other-swiper .swiper-button-prev',
                nextButton:'.J-buy-other-swiper .swiper-button-next',
                paginationClickable :true,
                slidesPerView:'4',
                spaceBetween : 20,
                calculateHeight : true,
                noSwiping : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : false,
            })
        }else{
            var swiper = new Swiper ('.J-buy-other-swiper .swiper-container', {
                prevButton:'.J-buy-other-swiper .swiper-button-prev',
                nextButton:'.J-buy-other-swiper .swiper-button-next',
                paginationClickable :true,
                slidesPerView:'1',
                calculateHeight : true,
                noSwiping : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : false,
            })
        }
        if($.browser.msie&&($.browser.version === "9.0")){
            $('.J-buy-other-swiper .swiper-button-prev').click(function(){
                swiper.swipePrev();
            })
            $('.J-buy-other-swiper .swiper-button-next').click(function(){
                swiper.swipeNext();
            })
        }
    }
    if($("#sub-map").length > 0){
        subMap()
        buySwiper()
    }

    /**购买-线下门店详情页  end*/


    /**探索colmo*/
    /*探索colmo-首屏视频按钮*/
    if($(".J-ts-video").find("video").length > 0){
        $(".J-ts-video").find(".J-ts-video-tit").show();
    }else{
        $(".J-ts-video").find(".J-ts-video-tit").hide();
    }

    /*探索colmo-科技美学*/
    function tskjSwiper(){
        var swiper = new Swiper ('.J-ts-kj-lb .swiper-container', {
            pagination: '.J-ts-kj-lb .pagination',
            paginationClickable :true,
            calculateHeight : true,
            noSwiping : true,
            autoplay:6000,
            autoplayDisableOnInteraction : false,
            loop : false,
            onSlideChangeStart: function(swiper){
                $(".J-ts-kj-btn ul li").eq(swiper.activeIndex).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
            },
        })
        var $_length = $(".J-ts-kj-lb .swiper-slide").length;
        if($_length > 1){
            $(".J-ts-kj-lb .swiper-pagination").show();
        }else{
            $(".J-ts-kj-lb .swiper-pagination").hide();
        }

        if($.browser.msie&&($.browser.version === "9.0")) {
            $(".J-ts-kj-btn ul li").click(function(){
                swiper.swipeTo($(this).index());
            })
        }else{
            $(".J-ts-kj-btn ul li").click(function(){
                swiper.slideTo($(this).index());
            })
        }
    }
    if($(".J-ts-kj-swiper").length > 0){
        tskjSwiper()
    }

    /*探索colmo-生活美学*/
    function tsshSwiper(){
        var swiper2 = new Swiper ('.J-ts-sh-lb .swiper-container', {
            prevButton:'.J-ts-sh-lb .swiper-button-prev',
            nextButton:'.J-ts-sh-lb .swiper-button-next',
            paginationClickable :true,
            calculateHeight : true,
            noSwiping : true,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : false,
            onSwiperCreated: function(mySwiper){
                var $_pageLen = $(".J-ts-sh-lb .pagination span");
                $_pageLen.each(function(){
                    var $_index = $(this).index() + 1;
                    $(this).html('0'+$_index);
                })
            },
            onSlideChangeStart: function(swiper){
                $(".J-ts-sh-top ul li").eq(swiper.activeIndex).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
            },
        })

        if($.browser.msie&&($.browser.version === "9.0")){
            $(".J-ts-kj-btn ul li").click(function(){
                swiper2.swipeTo($(this).index());
            })
            $(".J-ts-sh-top ul li").click(function(){
                swiper2.swipeTo($(this).index());
            })
            $('.J-ts-sh-lb .swiper-button-prev').click(function(){
                swiper2.swipePrev();
            })
            $('.J-ts-sh-lb .swiper-button-next').click(function(){
                swiper2.swipeNext();
            })
        }else{
            $(".J-ts-kj-btn ul li").click(function(){
                swiper2.slideTo($(this).index());
            })
            $(".J-ts-sh-top ul li").click(function(){
                swiper2.slideTo($(this).index());
            })
        }
    }
    if($(".J-ts-sh-swiper").length > 0){
        tsshSwiper()
    }
    /**探索colmo   end*/

    // $(".member-item h4").click(function(){
    //     $(this).siblings().slideToggle(200);
    // })


    /*场景页--轮播导航*/
    function navSlide(){
        if($(".J-cf-nav").length > 0){
            var $_len = $(".J-cf-nav").find(".swiper-slide").length;
            if($(window).width() > 1024){
                if($_len > 8){
                    $('.J-cf-nav .btn-prev').css('opacity','1');
                    $('.J-cf-nav .btn-next').css('opacity','1');
                }else{
                    $('.J-cf-nav .btn-prev').css('opacity','0');
                    $('.J-cf-nav .btn-next').css('opacity','0');
                }

                var swiperCf = new Swiper ('.J-cf-nav .swiper-container', {
                    prevButton:'.J-cf-nav .swiper-button-prev',
                    nextButton:'.J-cf-nav .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 8,
                    noSwiping : true,
                    noSwipingClass : 'noSwiping',
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                })
            }else if($(window).width() > 768){
                if($_len > 6){
                    $('.J-cf-nav .btn-prev').css('opacity','1');
                    $('.J-cf-nav .btn-next').css('opacity','1');
                }else{
                    $('.J-cf-nav .btn-prev').css('opacity','0');
                    $('.J-cf-nav .btn-next').css('opacity','0');
                }

                var swiperCf = new Swiper ('.J-cf-nav .swiper-container', {
                    prevButton:'.J-cf-nav .swiper-button-prev',
                    nextButton:'.J-cf-nav .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 6,
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                })
            }else{
                if($_len > 3){
                    $('.J-cf-nav .btn-prev').css('opacity','1');
                    $('.J-cf-nav .btn-next').css('opacity','1');
                }else{
                    $('.J-cf-nav .btn-prev').css('opacity','0');
                    $('.J-cf-nav .btn-next').css('opacity','0');
                }

                var swiperCf = new Swiper ('.J-cf-nav .swiper-container', {
                    prevButton:'.J-cf-nav .swiper-button-prev',
                    nextButton:'.J-cf-nav .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 3,
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                })
            }
            if($.browser.msie&&($.browser.version === "9.0")){
                $('.J-cf-nav .btn-prev').click(function(){
                    swiperCf.swipePrev();
                })
                $('.J-cf-nav .btn-next').click(function(){
                    swiperCf.swipeNext();
                })
            }
        }

    }
    navSlide();

    /*产品列表页--轮播导航*/
    function proNav(){
        if($(".J-bx-nav").length > 0){
            var $_len = $(".J-bx-nav").find(".swiper-slide").length;
            if($(window).width() > 1024){
                var swiper = new Swiper ('.bx-nav-swiper .swiper-container', {
                    prevButton:'.bx-nav-swiper .swiper-button-prev',
                    nextButton:'.bx-nav-swiper .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 6,
                    noSwiping : true,
                    noSwipingClass : 'noSwiping',
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                    observer:true,
                    observeParents:true,
                })
                if($.browser.msie&&($.browser.version === "9.0")){
                    $('.J-bx-nav .btn-prev').click(function(){
                        swiper.swipePrev();
                    })
                    $('.J-bx-nav .btn-next').click(function(){
                        swiper.swipeNext();
                    })
                }
            }else if($(window).width() > 768){
                var swiper = new Swiper ('.bx-nav-swiper .swiper-container', {
                    prevButton:'.bx-nav-swiper .swiper-button-prev',
                    nextButton:'.bx-nav-swiper .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 4,
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                    observer:true,
                    observeParents:true,
                })
                if($.browser.msie&&($.browser.version === "9.0")){
                    $('.J-bx-nav .btn-prev').click(function(){
                        swiper.swipePrev();
                    })
                    $('.J-bx-nav .btn-next').click(function(){
                        swiper.swipeNext();
                    })
                }
            }else{
                var swiper = new Swiper ('.bx-nav-swiper .swiper-container', {
                    prevButton:'.bx-nav-swiper .swiper-button-prev',
                    nextButton:'.bx-nav-swiper .swiper-button-next',
                    paginationClickable :true,
                    calculateHeight : true,
                    slidesPerView : 3,
                    // autoplay:5000,
                    autoplayDisableOnInteraction : false,
                    loop : false,
                    observer:true,
                    observeParents:true,
                })
                if($.browser.msie&&($.browser.version === "9.0")){
                    $('.J-bx-nav .btn-prev').click(function(){
                        swiper.swipePrev();
                    })
                    $('.J-bx-nav .btn-next').click(function(){
                        swiper.swipeNext();
                    })
                }
            }
            if($.browser.msie&&($.browser.version === "9.0")){
                if($(window).width() > 1024){
                    if($_len > 6){
                        $('.J-bx-nav .btn-prev').css('opacity','1');
                        $('.J-bx-nav .btn-next').css('opacity','1');
                    }else{
                        $('.J-bx-nav .btn-prev').css('opacity','0');
                        $('.J-bx-nav .btn-next').css('opacity','0');
                    }
                }else if($(window).width() > 768){
                    if($_len > 4){
                        $('.J-bx-nav .btn-prev').css('opacity','1');
                        $('.J-bx-nav .btn-next').css('opacity','1');
                    }else{
                        $('.J-bx-nav .btn-prev').css('opacity','0');
                        $('.J-bx-nav .btn-next').css('opacity','0');
                    }
                }else{
                    if($_len > 3){
                        $('.J-bx-nav .btn-prev').css('opacity','1');
                        $('.J-bx-nav .btn-next').css('opacity','1');
                    }else{
                        $('.J-bx-nav .btn-prev').css('opacity','0');
                        $('.J-bx-nav .btn-next').css('opacity','0');
                    }
                }
            }
        }
    }
    proNav();
    $(".J-bx-nav .sub-tit").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
            $(".J-bx-nav .bx-nav-swiper").addClass(_ACTIVE_);
        }else{
            $(this).removeClass(_ACTIVE_);
            $(".J-bx-nav .bx-nav-swiper").removeClass(_ACTIVE_);
        }
    })


    /*会员 兑换数量*/
    function exchange(){
        var $_base = $(".J-d-jf").text();
        var $_min = 1;
        var _max = 200;
        var $_t = $(".J-textBox");
        $_t.val($_min);
        $(".J-d-num .J-btn-reduce").attr('disabled',true).addClass("disabled");
        $(".J-d-num .J-btn-add").click(function(){
            $_t.val(Math.abs(parseInt($_t.val()))+1);
            if (parseInt($_t.val())!=1){
                $(".J-d-num .J-btn-reduce").attr('disabled',false).removeClass("disabled");
            };
            setTotal()
        })
        $(".J-d-num .J-btn-reduce").click(function(){
            $_t.val(Math.abs(parseInt($_t.val()))-1);
            if (parseInt($_t.val())==1){
                $(".J-d-num .J-btn-reduce").attr('disabled',true).addClass("disabled");
            };
            setTotal()
        })

        function setTotal(){
            $("#J-total").html((parseInt($_t.val())*$_base));
        }
        setTotal();

        /*点击显示订单详情*/
        $(".J-confirm").click(function(){
            if($(".J-hy-address").length > 0){
                var $pro_img = $(".J-take-mall-img img").attr('src');
                var $pro_name = $(".J-take-pro-tit").text();
                var $human = $(".J-a-name").text();
                var $tel = $(".J-a-tel").text();
                var $address = $(".J-a-add").text();
                var $total = $("#J-total").text();

                $(".J-popup-order").fadeIn(200);
                $(".J-popup-order .p-t-img img").attr('src',$pro_img);
                $(".J-popup-order .p-pro-name").html($pro_name);
                $(".J-popup-order .p-pro-integral span").html($total);
                $(".J-popup-order .s-shr").html($human);
                $(".J-popup-order .s-tel").html($tel);
                $(".J-popup-order .s-address").html($address);
            }else{
                $(".J-popup-order").fadeIn(200);
                $(".J-popup-order").find(".notLogged").show().siblings().hide();
            }
        });
        $(".J-popup-order .p-btn-cancel,.J-closeNot").click(function(){
            $(".J-popup-order").fadeOut(200);
        });

        $(".J-p-btn-confirm").on('click',function(){
            $(".J-popup-order .popup-success").show().siblings().hide();;
        });
        $(".J-closeSuccess").on('click',function(){
            $(".J-popup-order").fadeOut(200);
        });

    }
    exchange();

    /*新闻详情页复制*/
    $(".J-copy").click(function(){
        copyUrl($(this));
        $(".J-copyCon").fadeIn(200);
    })
    $(".J-copyCon").on("click",".closeCopy",function(e){
        $(".J-copyCon").fadeOut(200);
    })
    function copyUrl (obj){
        if($('#urlText').length == 0){
            // 创建input
            obj.after('<input id="urlText" style="position:fixed;top:-200%;left:-200%;" type="text" value=' + window.location.href + '>');
        }
        $('#urlText').select(); //选择对象
        document.execCommand("Copy"); //执行浏览器复制命令

    }

    //会员--入会有礼--会员等级切换
    $(".J-club-table-tit li").click(function(){
        var $_index = $(this).index();
        var $dataset = $(this).attr("data-show");
        $(this).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
        $(".J-club-table-con .club-grade-item").eq($_index).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
        var $arr = $dataset.trim().split(',');
        $(".J-club-equity dl dd").each(function () {
            $(this).addClass("gray");
            for(var i in $arr){
                var $ii = $arr[i];
                if(($ii-1) === $(this).index()){
                    $(this).removeClass("gray");
                }
            }
        });
    })
    if($(".J-club-table-tit").length > 0){
        $(".J-club-table-tit li:first-child").click();
    }
    $(".J-club-equity dd,.J-infor-index-equity dd,.J-reg-ly-item").click(function(){
        $(this).find(".J-club-equity-fixed").fadeIn(100);
    })
    $(".J-club-equity-fixed a").click(function(event){
        $(".J-club-equity-fixed").fadeOut(100);
        event.stopPropagation();
    });

    /*会员-入会有礼-产品列表*/
    $(".J-ex-intervals").find("input").focus(function(){
        $(this).attr('placeholder','');
    });
    $(".J-ex-intervals").find("input").bind('blur',function(){
        $(this).attr('placeholder','0');
    });

    /*品牌活动--最新动态点击加载*/
    function listShowMore(){
        var $_content = [];
        var $loadMore = {
            _default : 4,
            _loading : 4,
            init:function(){
                var $lis = $(".J-data-list .activity-item");
                $(".J-activity-list").html("");
                for(var n=0; n<$loadMore._default; n++){
                    $lis.eq(n).appendTo(".J-activity-list");
                }
                for(var i=$loadMore._default; i<$lis.length; i++){
                    $_content.push($lis.eq(i));
                }
                $(".J-data-list").html("");
            },
            load:function(){
                // var mLis = $(".re-table-ul .listShow li").length;
                for(var i = 0; i<$loadMore._loading; i++){
                    var $target = $_content.shift();
                    if(!$target){
                        $(".J-activity-more a").hide();
                        break;
                    }
                    $(".J-activity-list").append($target);
                }
            }
        }
        $loadMore.init();
        $(".J-activity-more a").click(function(){
            $loadMore.load();
        })

    }
    listShowMore()

    //个人中心点击事件
    /*个人中心头像弹框*/
    $(".J-infor-head").click(function(){
        $(".J-head-popup").fadeIn(100);
    })
    /*关闭设置头像弹框*/
    $(".J-head-popup .J-closeHead,.J-cancel").click(function(){
        $(".J-head-popup").fadeOut(100);
    })
    /*打开信息完善内容*/
    $(".J-editOpen a").click(function(){
        $(".infor-index-con").hide();
        $(".infor-data-con").css({'height':'auto','opacity':'1','overflow':'inherit'})
    })
    /*关闭信息完善内容*/
    $("#J-inforReturn").click(function(){
        $(".infor-index-con").show();
        $(".infor-data-con").css({'height':'0','opacity':'0','overflow':'hidden'})
        $(".J-tel").val('');
        $(".J-codeInt").val('');
        $(".J-emptyName").val('');
        $(".J-city").val('');
        $(".J-gender").val('');
        $(".J-date").val('');
        $(".J-infor-form").find(".tips").text("").removeClass("success");
    })

    /*个人中心-显示验证码*/
    $(".itemTel").find(".J-tel").attr("readonly","readonly");
    $(".itemTel").find(".J-btnTel").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
            $(this).text("取消修改");
            $(".itemTel").find(".J-tel").removeAttr("readonly");
            $(".itemYzm").show();
        }else{
            $(this).removeClass(_ACTIVE_);
            $(this).text("修改手机号码");
            $(".itemTel").find(".J-tel").attr("readonly","readonly");
            $(".itemYzm").hide();
        }
    })
    /*个人中心--信息完善--兴趣爱好选择*/
    $(".J-infor-input-con ol li").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_);
        }else{
            $(this).removeClass(_ACTIVE_);
        }
    })
    /*个人中心--信息完善--生日*/
    if($(".J-infor-form").length > 0){
        $('.J-date').jHsDate();
        $('.J-date').click(function(){
            var $_w = $(this).width();
            console.log($_w)
            $("#datehsdate").width($_w);
        })
    }
    /*个人中心--信息完善--性别选择*/
    $(".J-gender").click(function(){
        $(this).siblings().show();
        $("#datehsdate").remove();
        event.stopPropagation();
    });
    $(".J-gender-ul li").click(function(){
        var $_txt = $(this).find("a").text();
        $(".J-gender").attr('value',$_txt);
        $(".J-gender").siblings(".tips").text("").addClass("success");
    });
    $(document).click(function(event){
        $(".J-gender-ul").hide();
        event.stopPropagation();
    });
    /*个人中心--信息完善--城市选择*/
    // $(".J-city").click(function(e){
    //     SelCity(this,e);
    // });
    if($(".city-picker-selector").length > 0){
    $('#city-picker-search').cityPicker({
        dataJson: cityData,
        renderMode: true,
        search: true,
        linkage: true
    });
    }


    /*个人中心--会员等级及权益*/
    function regSwiper(){
        if($(".J-register").length > 0){
            var swiper = new Swiper('.J-reg-swiper .swiper-container',{
                prevButton:'.J-reg-swiper .swiper-button-prev',
                nextButton:'.J-reg-swiper .swiper-button-next',
                slidesPerView : 1,
                calculateHeight : true,
                autoplay : false,
                loop:false,
                onInit: function(swiper){
                    $(".J-reg-page .reg-page-item").eq(swiper.activeIndex).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
                    var $_this = $(".J-reg-swiper .swiper-slide-active").attr("data-show");
                    var $arr = $_this.trim().split(',');
                    $(".J-reg-ly-list .J-reg-ly-item").each(function () {
                        $(this).addClass("gray");
                        for(var i in $arr){
                            var $ii = $arr[i];
                            if(($ii-1) === $(this).index()){
                                $(this).removeClass("gray");
                            }
                        }
                    });
                },
                onSlideChangeStart: function(swiper){
                    $(".J-reg-page .reg-page-item").eq(swiper.activeIndex).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
                    var $_this = $(".J-reg-swiper .swiper-slide-active").attr("data-show");
                    var $arr = $_this.trim().split(',');
                    $(".J-reg-ly-list .J-reg-ly-item").each(function () {
                        $(this).addClass("gray");
                        for(var i in $arr){
                            var $ii = $arr[i];
                            if(($ii-1) === $(this).index()){
                                $(this).removeClass("gray");
                            }
                        }
                    });

                    var $_slideIndex = swiper.activeIndex + 1;

                    if($_slideIndex > 1){
                        $(".J-reg-btn-prev").show();
                    }else{
                        $(".J-reg-btn-prev").hide();
                    }
                    // if($_slideIndex === 1){
                    //     $(".J-reg-page .reg-page-item:lt(1)").addClass(_ACTIVE_);
                    //     $(".J-reg-page .reg-page-item:gt(0)").removeClass(_ACTIVE_);
                    // }else if($_slideIndex === 2){
                    //     $(".J-reg-page .reg-page-item:lt(2)").addClass(_ACTIVE_);
                    //     $(".J-reg-page .reg-page-item:gt(1)").removeClass(_ACTIVE_);
                    // }else if($_slideIndex === 3){
                    //     $(".J-reg-page .reg-page-item:lt(3)").addClass(_ACTIVE_);
                    //     $(".J-reg-page .reg-page-item:gt(2)").removeClass(_ACTIVE_);
                    // }else if($_slideIndex === 4){
                    //     $(".J-reg-page .reg-page-item:lt(4)").addClass(_ACTIVE_);
                    //     $(".J-reg-page .reg-page-item:gt(3)").removeClass(_ACTIVE_);
                    // }else if($_slideIndex === 5){
                    //     $(".J-reg-page .reg-page-item:lt(5)").addClass(_ACTIVE_);
                    //     $(".J-reg-page .reg-page-item:gt(4)").removeClass(_ACTIVE_);
                    // }else{
                    //     $(".J-reg-btn-prev").hide();
                    // }

                    if($_slideIndex >= 5){
                        $(".J-reg-btn-next").hide();
                    }else{
                        $(".J-reg-btn-next").show();
                    }

                    $(".J-reg-hi-ws .reg-hi-ws-item").eq(swiper.activeIndex).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
                },
            })

            if($.browser.msie&&($.browser.version === "9.0")){
                $('.J-reg-btn-prev').on('click', function(e){
                    e.preventDefault();
                    swiper.swipePrev();
                })
                $('.J-reg-btn-next').on('click', function(e){
                    e.preventDefault();
                    swiper.swipeNext();
                })
                $(".J-reg-page .reg-page-item").click(function(){
                    swiper.swipeTo($(this).index());
                })
            }else{
                $(".J-reg-page .reg-page-item").click(function(){
                    swiper.slideTo($(this).index());
                })
            }

        }
    }
    regSwiper()

    /*个人中心移动端左侧导航下拉*/
    $(".infor-nav-item").find("dt").click(function(){
        if($(window).width() < 768){
            if(!$(this).hasClass(_ACTIVE_)){
                $(this).addClass(_ACTIVE_);
                $(this).siblings("dd").slideDown(100);
                $(this).parents(".infor-nav-item").siblings().find("dt").removeClass(_ACTIVE_);
                $(this).parents(".infor-nav-item").siblings().find("dd").slideUp(100);
            }else{
                $(this).removeClass(_ACTIVE_);
                $(this).siblings("dd").slideUp(100);
            }
        }
    })

    /*个人中心-积分商城订单*/
    $(".J-order-list").each(function(){
        var $childItem = $(this).find(".order-item").length;
        if($childItem > 0){
            $(this).find(".J-noData-con").hide();
        }else{
            $(this).find(".J-noData-con").show();
        }
    })

    /*个人中心-物流信息*/
    $(".J-logistics a").click(function(){
        $(".logistics-popup").addClass(_ACTIVE_);
    })
    $(".logistics-popup .close").click(function(){
        $(".logistics-popup").removeClass(_ACTIVE_);
    })
    /*入会有礼 点击显示二维码*/
    $("#J-list").on('click','.J-qr',function(){
        if($(window).width() > 768){
            $(this).parents(".club-dh-item").find(".dh-QR").addClass(_ACTIVE_);
            return false;
        }

    })
    $("#J-list").on('mouseleave','.club-dh-item',function(){
        $(this).find(".dh-QR").removeClass(_ACTIVE_);
    })
    /*入会有礼-产品兑换 二维码*/
    $(".J-xq").click(function(){
        $(".take-mall-img").addClass(_ACTIVE_);
    })
    $(".take-mall-img").on('mouseleave',function(){
        $(this).removeClass(_ACTIVE_);
    })



});


//当页面尺寸改变时执行
$(window).resize(function(){

    $("#PoPy").remove();
    $("#datehsdate").remove();
    $("#PoPx").remove();


});

$(function(){

    /*注册页面-是否阅读协议*/
    $(".J-checkBox").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(this).addClass(_ACTIVE_)
        }else{
            $(this).removeClass(_ACTIVE_)
        }
    });

    //登录注册手机号码验证
    var $formVerification = {

        //验证是否为空
        'isEmpty':function isEmpty(str){
            return str.length == 0;
        },

        //验证手机号
        'isMobile': function isMobile(str) {
            return (/^[1][3,4,5,7,8]\d{9}$/.test(str)
            );
        },

        //验证邮箱
        'isEmail': function isEmail(str) {
            return (/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(str)
            );
        },
    }



    var $tel = $(".J-tel"),
        $codeInt = $(".J-codeInt"),
        $btncode = $(".J-btncode"),
        $emptyName = $(".J-emptyName"),
        $emptyAddress = $(".input-price"),
        $gender = $(".J-gender"),
        $birthday = $(".J-date"),
        $Countdown = 60;

    //手机号校验
    $tel.on({
        focus: yztel,
        blur: yztel,
        keyup: yztel,
        keydown: yztel
    });
    function yztel() {
        var $_CurrentEle = $tel,
            $_Group = $_CurrentEle.parents('.J-tel-p'),
            $_Value = $_CurrentEle.val();

        if ($formVerification.isMobile($_Value)) {
            $_Group.removeClass(_ACTIVE_).find('.tips').text('').addClass('success');
            $(".J-btncode").removeClass("clickable");
        } else {
            if ($formVerification.isEmpty($_Value)) {
                $_Group.addClass(_ACTIVE_).find('.tips').text('手机号码不能为空！').removeClass('success');
            } else {
                $_Group.addClass(_ACTIVE_).find('.tips').text('请输入正确的联系电话！').removeClass('success');
            }
        }
    }

    //姓名是否为空
    $emptyName.on({
        focus: yzName,
        blur: yzName,
        keyup: yzName,
        keydown: yzName
    });
    function yzName() {
        var $_CurrentEle = $emptyName,
            $_Group = $_CurrentEle.parents('.J-add-input-con,.J-infor-input-con'),
            $_Value = $_CurrentEle.val();
        if ($formVerification.isEmpty($_Value)) {
            $_Group.addClass(_ACTIVE_).find('.tips').text('请填写姓名！').removeClass('success');
        } else {
            $_Group.addClass(_ACTIVE_).find('.tips').text('').addClass('success');
        }
    }

    //性别是否为空
    $gender.on({
        focus: yzGender,
        blur: yzGender,
        keyup: yzGender,
        keydown: yzGender
    });
    function yzGender() {
        var $_CurrentEle = $gender,
            $_Group = $_CurrentEle.parents('.J-infor-input-con'),
            $_Value = $_CurrentEle.val();
        if ($formVerification.isEmpty($_Value)) {
            $_Group.addClass(_ACTIVE_).find('.tips').text('请选择性别！').removeClass('success');
        } else {
            $_Group.addClass(_ACTIVE_).find('.tips').text('').addClass('success');
        }
    }

    //生日是否为空
    $birthday.on({
        focus: yzBirthday,
        blur: yzBirthday,
        keyup: yzBirthday,
        keydown: yzBirthday
    });
    function yzBirthday() {
        var $_CurrentEle = $birthday,
            $_Group = $_CurrentEle.parents('.J-infor-input-con'),
            $_Value = $_CurrentEle.val();
        if ($formVerification.isEmpty($_Value)) {
            $_Group.addClass(_ACTIVE_).find('.tips').text('请选择您的生日！').removeClass('success');
        } else {
            $_Group.addClass(_ACTIVE_).find('.tips').text('').addClass('success');
        }
    }

    //收货地址是否为空
    $emptyAddress.on({
        focus: yzAddress,
        blur: yzAddress,
        keyup: yzAddress,
        keydown: yzAddress,
        change:yzAddress
    });

    function yzAddress() {
        var $_CurrentEle = $emptyAddress,
            $_Group = $_CurrentEle.parents('#city-picker-search'),
            $_Value = $_CurrentEle.val();

        if ($formVerification.isEmpty($_Value)) {
            $_Group.addClass(_ACTIVE_).siblings('.tips').text('请选择收货地址！').removeClass('success');
        } else {
            $_Group.addClass(_ACTIVE_).siblings('.tips').text('').addClass('success');
        }
    }
    $(document).on("click",".caller",function(){
        $("#city-picker-search").siblings(".tips").text('').addClass('success');
    });

    //获取短信验证码
    $codeInt.on({
        focus: yzcodeInt,
        blur: yzcodeInt,
        keyup: yzcodeInt,
        keydown: yzcodeInt
    });

    function yzcodeInt() {
        var $_CurrentEle = $codeInt,
            $_Group = $_CurrentEle.parents('.J-yzm-p'),
            $_Value = $_CurrentEle.val();
        var $mess = '888888';
        if ($_Value === $mess) {
            $_Group.removeClass(_ACTIVE_).find('.tips').text('').addClass('success');
            $(".J-signIn").removeClass("clickable");
            $(".J-register").removeClass("clickable");
        } else {
            $(".J-signIn").addClass("clickable");
            $(".J-register").addClass("clickable");
            if ($formVerification.isEmpty($_Value)) {
                $_Group.addClass(_ACTIVE_).find('.tips').text('短信验证码不能为空！').removeClass('success');
            } else {
                $_Group.addClass(_ACTIVE_).find('.tips').text('请输入正确的短信验证码！').removeClass('success');
            }
        }
    }

    $btncode.on('click', function () {
        if ($btncode.hasClass(_ACTIVE_)) return;
        $btncode.addClass(_ACTIVE_).text('短信发送中');
        setTimeout(function () {
            countdownStart();
        }, 1000);
    });

    //倒计时方法
    function countdownStart() {
        if ($Countdown === 0) {
            $btncode.removeClass(_ACTIVE_).text('获取验证码');
            $Countdown = 60;
            return;
        } else {
            $btncode.addClass(_ACTIVE_).text($Countdown + 's后重新发送');
            $Countdown--;
        }
        timer = setTimeout(countdownStart, 1000);
    }

    //是否阅读协议
    function agreement() {
        if(!$(".J-agreement .J-checkBox").hasClass(_ACTIVE_)){
            $(".J-agreement .tips").text("请阅读并同意COLMO的协议政策！");
        }else{
            $(".J-agreement .tips").text("");
            $(".J-agreement .tips").addClass('success');
        }
    }
    $(".J-agreement .J-checkBox").click(function(){
        if(!$(this).hasClass(_ACTIVE_)){
            $(".J-agreement .tips").text("请阅读并同意COLMO的协议政策！");
            $(".J-agreement .tips").removeClass('success');
        }else{
            $(".J-agreement .tips").text("");
            $(".J-agreement .tips").addClass('success');
        }
    })

    //登录
    $(".J-signIn").click(function(){

        //再次校验
        var $_exists = '13333333333';
        var $_offTel = $(".J-tel").val();
        if($(".login-p .success").length >= 2){
            if($_exists === $_offTel){
                var $_txt = '登录成功';
                var $_html = '<div class="successPopup">'+
                    '<div class="successPopup-con">'+
                    '<img src="static/img/success.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a href="index.html">确定</a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else{
                var $_txt = '您填写的信息已超时，请检查信息后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }
        }else{
            if (!$tel.siblings('.tips').hasClass('success')) {
                yztel();
            }
            if (!$codeInt.siblings('.tips').hasClass('success')) {
                yzcodeInt();
            }
        }

    });
    //注册
    $(".J-register").click(function(){
        //再次校验
        var $_exists = '13333333333';
        var $_Noexists = '13333333331';
        var $_offTel = $(".J-tel").val();

        if($(".register-int .success").length >= 3){
            if($_exists === $_offTel){
                var $_txt = '注册成功';
                var $_html = '<div class="successPopup">'+
                    '<div class="successPopup-con">'+
                    '<img src="static/img/success.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a href="login.html">立即登录</a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else if($_Noexists === $_offTel){
                var $_txt = '您的手机号已被注册过';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else{
                var $_txt = '您填写的信息已超时，请检查信息后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }
        }else{
            if (!$tel.siblings('.tips').hasClass('success')) {
                yztel();
            }
            if (!$codeInt.siblings('.tips').hasClass('success')) {
                yzcodeInt();
            }
            agreement();
        }
    });
    /*会员-入会有礼-地址添加*/
    $("#J-addressBtn").click(function(){
        var $_exists = '13333333333';
        var $_Noexists = '13333333331';
        var $_offTel = $(".J-tel").val();
        if($(".J-add-form .success").length >= 3){
            if($_exists === $_offTel){
                var $_txt = '保存成功';
                var $_html = '<div class="successPopup">'+
                    '<div class="successPopup-con">'+
                    '<img src="static/img/success.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="J-save-qd" href="huiyuan-take-receipt.html">确定</a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else if($_Noexists === $_offTel){
                var $_txt = '您填写的信息不规范，请排查后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else{
                var $_txt = '您填写的信息已超时，请检查信息后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }
        }else{
            if (!$emptyName.siblings('.tips').hasClass('success')) {
                yzName();
            }
            if (!$tel.siblings('.tips').hasClass('success')) {
                yztel();
            }
            if(!$emptyAddress.siblings('.tips').hasClass('success')){
                yzAddress();
            }
        }
    });
    /*个人中心-地址管理*/
    $("#J-takeBtn").click(function(){
        var $_exists = '13333333333';
        var $_Noexists = '13333333331';
        var $_offTel = $(".J-tel").val();
        if($(".J-add-form .success").length >= 3){
            if($_exists === $_offTel){
                var $_txt = '保存成功';
                var $_html = '<div class="successPopup">'+
                    '<div class="successPopup-con">'+
                    '<img src="static/img/success.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="J-save-qd" href="javascript:;">确定</a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else if($_Noexists === $_offTel){
                var $_txt = '很遗憾，您的信息填写不符合规范，请重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else{
                var $_txt = '您填写的信息已超时，请检查信息后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }
        }else{
            if (!$emptyName.siblings('.tips').hasClass('success')) {
                yzName();
            }
            if (!$tel.siblings('.tips').hasClass('success')) {
                yztel();
            }
            if(!$emptyAddress.siblings('.tips').hasClass('success')){
                yzAddress();
            }
        }
    })
    /*个人中心-信息完善*/
    $(".j-saveBtn").click(function(){
        var $_exists = '13333333333';
        var $_Noexists = '13333333331';
        var $_offTel = $tel.val();

        if($(".J-infor-form .success").length >= 5){
            if($_exists === $_offTel){
                var $_txt = '保存成功';
                var $_html = '<div class="successPopup">'+
                    '<div class="successPopup-con">'+
                    '<img src="static/img/success.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="J-save-qd" href="javascript:;">确定</a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else if($_Noexists === $_offTel){
                var $_txt = '很遗憾，您的信息填写不符合规范，请重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }else{
                var $_txt = '您填写的信息已超时，请检查信息后重新提交';
                var $_html = '<div class="failPopup">'+
                    '<div class="failPopup-con">'+
                    '<img src="static/img/failIcon.png" alt="">'+
                    '<p>'+$_txt+'</p>'+
                    '<a class="closeFail" href="javascript:;"></a>'+
                    '</div>'+
                    '</div>';
                $('body').append($_html);
            }
        }else{
            if (!$emptyName.siblings('.tips').hasClass('success')) {
                yzName();
            }
            if (!$tel.siblings('.tips').hasClass('success')) {
                yztel();
            }
            if(!$gender.siblings('.tips').hasClass('success')){
                yzGender();
            }
            if(!$birthday.siblings('.tips').hasClass('success')){
                yzBirthday();
            }
            if(!$emptyAddress.siblings('.tips').hasClass('success')){
                yzAddress();
            }
        }
    });

    /*提示类弹框点击确定、关闭或退出信息填写时，清空input信息*/
    $("body").on("click",".failPopup a,.J-save-qd",function(){
        $(".failPopup").remove();
        $(".J-tel").val('');
        $(".J-codeInt").val('');
        $(".J-emptyName").val('');
        $(".J-city").val('');
        $(".J-gender").val('');
        $(".J-date").val('');
    });
    $("body").on("click",".successPopup a",function(){
        $(".successPopup").remove();
    })



});

