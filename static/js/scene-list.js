var PAGE_SIZE = 12;
var CATE_TOTAL_TITLE = sceneObj.sceneName+"全部产品";
var PAGE_PERVIEW = 7;//当总页数超过这个数时，显示省略号

var categoryModule = {
    $categoryList:$('.J-categories-item'),
    init:function(){
        this.bindEvents();
    },
    bindEvents:function(){
        var me = this;
        me.$categoryList.click(function(){
            sceneObj.uuid = $(this).attr('data-uuid');
            load(1)
        })
    }
}

$(document).ready(function () {
    if($(".cf-allpro").length > 0){
        load(1);
    }
    categoryModule.init()
});
var first = true;
function scrollTo(element,speed) {
    if(!speed)speed = 100;
    if(!element) $("html,body").animate({scrollTop:0},speed);

    else{
        if(element.length>0){
            $("html,body").animate({scrollTop:$(element).offset().top-101},speed);
        }
    }
}
function load(page) {
    var url = getUrl(page);
    $('#loading').show();
    if(!first)scrollTo('#toper',500);
    first=false;
    $('.cf-allpro').each(function () {
        var c = this;
        setTimeout(function () {
            $(c).html('');
        },260);
        var timer = setTimeout(function () {
            $.get(url, function (result) {
                var data = result.data;
                var pageCount = Math.ceil(data.total/PAGE_SIZE);
                data.now=page;
                $(c).append($("<h3 class=\"sub-tit\"></h3>").text(CATE_TOTAL_TITLE));
                var container = $("<div class=\"sub-container\"></div>");
                $(c).append(container);
                var list = $("<div class=\"cf-pro-list clear\"></div>");
                $(container).append(list);
                var code = [];
                for (var i = 0; i < data.list.length; i++) {
                    var row = data.list[i];
                    var html = '<div class="cf-pro-item">' +
                        '<a href="' + row.pageLink + '">' +
                        '<img src="' + row.productImageHome + '">' +
                        '<span class="nm">' + row.productName + '</span>';
                    if(row.productSpec){
                        html += '<span class="xh">' + row.productSpec + '</span>';
                    }
                    html += '</a></div>';
                    code.push(html);
                }
                $(list).html(code.join(''));
                var nav = [];
                var navContainer = $("<div class=\"page-number\"></div>");
                var navUl = $('<ul></ul>');
                $(c).append(navContainer);
                $(navContainer).append(navUl);
                if(data.now>1)  nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
                //显示最后一页
                var pageCountStr = pageCount,pageCountClass='';
                if(pageCount < 9){
                    pageCountStr = '0'+ pageCount;
                }
                if(pageCount===data.now) pageCountClass='class="active"';
                var lastPageStr = '<li '+ pageCountClass + '><a href="javascript:load('+pageCount+');">'+pageCountStr+'</a></li>';
                if (pageCount <= PAGE_PERVIEW){
                    for(var i=1;i<=pageCount;i++){
                        var str = i < 9 ? ('0'+i) : i;
                        var now = '';
                        if(i===data.now) now='class="active"';
                        nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                    }
                }else{
                    if (data.now <= PAGE_PERVIEW-2){
                        for(var i=1;i<=PAGE_PERVIEW-2;i++){
                            var str = i < 9 ? ('0'+i) : i;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    } else if(data.now >= pageCount-2) {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=pageCount-PAGE_PERVIEW+3;i<=pageCount-1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push(lastPageStr)
                    } else {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=data.now-1;i<=data.now+1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    }
                }

                $(navUl).html(nav.join(''));
                if(data.now < pageCount) navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
            });
            $('#loading').hide();
            clearTimeout(timer);
        }, 2000)
    });
}
function getUrl(page){
    return config.urlPrefix + '/product/listProductByCate?productCate='+encodeURIComponent(sceneObj.uuid) + '&pageIndex=' + (page-1) +'&pageSize=' + PAGE_SIZE;
}