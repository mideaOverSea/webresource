var PAGE_SIZE = 12;
// var CATE_TOTAL_TITLE = categoryObj.cateName+"全部产品";
var CATE_TOTAL_TITLE = "全部产品";
var PAGE_PERVIEW = 7;//当总页数超过这个数时，显示省略号

$(document).ready(function () {
    if($(".J-cf-allpro").length > 0){
        load(1);
    }
    $(".J-proType dd").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
    })
});
var first = true;
function scrollTo(element,speed) {
    // if($(".cf-allpro").length > 0){
    if(!speed)speed = 100;
    if(!element) $("html,body").animate({scrollTop:0},speed);
    else{
        if(element.length>0){
            $("html,body").animate({scrollTop:$(element).offset().top-101},speed);
        }
    }
}
function load1(page){
  $(".J-proType dd").removeClass("active");
  $(".J-proType dd:first").addClass("active");
  load(page);
}
function load(page) {
    var url = getUrl(page);
    $('#loading').show();
    if(!first)scrollTo('#toper',500);
    first=false;
    $('.J-cf-allpro').each(function () {
        var c = this;
        setTimeout(function () {
            $(c).html('');
        },260);
        var timer = setTimeout(function () {
            $.get(url, function (result) {
                var data = result.data;
                var pageCount = Math.ceil(data.total/PAGE_SIZE);
                data.now=page;
                $(c).append($("<h3 class=\"sub-tit\"></h3>").text(CATE_TOTAL_TITLE));
                var container = $("<div class=\"sub-container\"></div>");
                $(c).append(container);
                var list = $("<div class=\"cf-pro-list clear\"></div>");
                $(container).append(list);
                var code = [];
                for (var i = 0; i < data.list.length; i++) {
                    var row = data.list[i];
                    var html = '<div class="cf-pro-item" id="pro-'+i+'">' +
                        '<a href="' + row.pageLink + '">' +
                        '<img src="' + row.productImageHome + '">' +
                        '<div class="proName">'+
                        '<span class="nm">' + row.productName + '</span>';
                    if(row.productSpec){
                        html += '<span class="xh">' + row.productSpec + '</span></div>';
                    }
                    html += '</a>' +
                                '<div class="pro-btnBox">' +
                                    '<a class="J-fun-sc fun-sc" href="javascript:;"><i></i><span>收藏</span></a>' +
                                    '<a class="J-fun-db fun-db" href="javascript:;" auto="0" data-img="'+$row.src+'" data-name="'+$row.title+'" data-model="'+$row.type +'"><i></i><span>加入对比</span></a>' +
                                '</div>' +
                            '</div>';
                    code.push(html);
                }
                $(list).html(code.join(''));
                var nav = [];
                var navContainer = $("<div class=\"m-page-number\"></div>");
                var navUl = $('<ul></ul>');
                $(c).append(navContainer);
                $(navContainer).append(navUl);
                if(data.now>1)  nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
                //显示最后一页
                var pageCountStr = pageCount,pageCountClass='';
                if(pageCount < 9){
                    pageCountStr = '0'+ pageCount;
                }
                if(pageCount===data.now) pageCountClass='class="active"';
                var lastPageStr = '<li '+ pageCountClass + '><a href="javascript:load('+pageCount+');">'+pageCountStr+'</a></li>';
                if (pageCount <= PAGE_PERVIEW){
                    for(var i=1;i<=pageCount;i++){
                        var str = i < 9 ? ('0'+i) : i;
                        var now = '';
                        if(i===data.now) now='class="active"';
                        nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                    }
                }else{
                    if (data.now <= PAGE_PERVIEW-2){
                        for(var i=1;i<=PAGE_PERVIEW-2;i++){
                            var str = i < 9 ? ('0'+i) : i;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    } else if(data.now >= pageCount-2) {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=pageCount-PAGE_PERVIEW+3;i<=pageCount-1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push(lastPageStr)
                    } else {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=data.now-1;i<=data.now+1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    }
                }

                // if(data.now<5){
                //     for(var i=1;i<=pageCount-1;i++){
                //         var str = i < 9 ? ('0'+i) : i;
                //         var now = '';
                //         if(i===data.now) now='class="active"';
                //         nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                //     }
                //     if(pageCount > 5){
                //         nav.push('<li>...</li>');
                //     }
                // }else if(data.now> pageCount-5){
                //     nav.push('<li><a href="javascript:load(1);">01</a></li>');
                //     nav.push('<li>...</li>');
                //     for(var i=pageCount-5;i<=pageCount-1;i++){
                //         var str = i+'';
                //         if(str.length<=1) str='0'+str;
                //         var now = '';
                //         if(i===data.now) now='class="active"';
                //         nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                //     }
                // }else{
                //     nav.push('<li><a href="javascript:load(i);">01</a></li>');
                //     nav.push('<li>...</li>');
                //     for(var i=data.now-1;i<data.now+4;i++){
                //         var str = i+'';
                //         if(str.length<=1) str='0'+str;
                //         var now = '';
                //         if(i===data.now) now='class="active"';
                //         nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                //     }
                //     nav.push('<li>...</li>');
                // }
                $(navUl).html(nav.join(''));
                if(data.now < pageCount) navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
                $('#loading').hide();
                clearTimeout(timer);
            });
        }, 2000)
    });
}
function getUrl(page){
    // return "static/json/data" + page + ".json";
    return config.urlPrefix + '/product/listProductByCate?productCate='+encodeURIComponent(categoryObj.uuid) + '&pageIndex=' + (page-1) +'&pageSize=' + PAGE_SIZE;
}