var PAGE_SIZE = newsObj.type == "4" ? 6 : 12;
var PAGE_PERVIEW = 7;//当总页数超过这个数时，显示省略号

//定义时间转换函数
function add0(m) {
    return m < 10 ? '0' + m : m
}
function formatYYMMDD(timestamp) {
    //timestamp是整数，否则要parseInt转换,不会出现少个0的情况
    var time = new Date(timestamp);
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var date = time.getDate();
    return year + '-' + add0(month) + '-' + add0(date);
}

//根据模板类型，生成不同的item html string,type:1 品牌新闻 ，2品牌活动，3全球超级个体，4私享活动
function formatItemHtmlStr(type,row){
    var htmlStr = '';
    switch(type){
        case "1":
        case "3":
            htmlStr += '<li><a href="'+row.link+'">'+
                '<div class="news-item-img"><img src="'+row.newsImageList+'"></div>'+
                '<div class="news-item-con">'+
                (row.newsName?'<h4>'+row.newsName+'</h4>':'')+
                (row.newsPublishtime?'<p class="item-p"><span class="item-p-sp">'+formatYYMMDD(row.newsPublishtime)+'</span></p>':'')+
                '</div></a></li>';
            break;
        case "4":
            htmlStr += '<li class="private-item"><a href="'+((row.priAct && row.priAct.priLink) || row.link)+'">'+
                '<div class="private-item-img"><img src="'+row.newsImageList+'">'+
                ((row.priAct&& row.priAct.tag)?'<span class="state">'+row.priAct.tag+'</span>':'')+
                '</div>'+
                '<div class="private-item-txt">'+
                (row.newsName?'<div class="private-item-tit">'+row.newsName+'</div>':'')+
                (row.priAct && row.priAct.subTile?'<p class="privateP">'+row.priAct.subTile+'</p>':'')+
                '<span class="xq-sp">查看详情</span>'+
                '</div>'+
                '</a></li>';
            break;
    }
    return htmlStr
}


$(document).ready(function() {
    if($("#list").length > 0){
        load(1);
    }
});
var first=true;
function scrollTo(element,speed) {
    if(!speed)speed = 100;
    if(!element) $("html,body").animate({scrollTop:0},speed);
    else{
        if(element.length>0){
            $("html,body").animate({scrollTop:$(element).offset().top-140},speed);
        }
    }
}
function load(page) {
    var url = getUrl(page);
    $('#loading').show();
    if(!first)scrollTo('#loading',500);
    first=false;
    $('#list').each(function () {
        var list = this;
        setTimeout(function () {
            $(list).html('');
        },260);
        var listCode=[];
        setTimeout(function () {
            $.get(url ,function (result) {
                var data = result.data || [];
                var pageCount = Math.ceil(data.total/PAGE_SIZE);
                data.now=page;
                $('#loading').hide();
                if(data.total<1){
                    $('#list-none').show();
                }else{
                    $(list).show();
                }
                for (var i = 0; i < data.list.length; i++) {
                    var row = data.list[i];
                    listCode.push(formatItemHtmlStr(newsObj.type,row))
                }
                $(list).html(listCode.join(''));
                var nav = [];
                var navUl = $('#list-page');
                if(data.now>1)  nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
                var pageCountStr = pageCount,pageCountClass='';
                if(pageCount < 10){
                    pageCountStr = '0'+ pageCount;
                }
                if(pageCount===data.now) pageCountClass='class="active"';
                var lastPageStr = '<li '+ pageCountClass + '><a href="javascript:load('+pageCount+');">'+pageCountStr+'</a></li>';

                if (pageCount <= PAGE_PERVIEW){
                    for(var i=1;i<=pageCount;i++){
                        var str = i < 9 ? ('0'+i) : i;
                        var now = '';
                        if(i===data.now) now='class="active"';
                        nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                    }
                }else{
                    if (data.now <= PAGE_PERVIEW-2){
                        for(var i=1;i<=PAGE_PERVIEW-2;i++){
                            var str = i < 9 ? ('0'+i) : i;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    } else if(data.now >= pageCount-2) {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=pageCount-PAGE_PERVIEW+3;i<=pageCount-1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push(lastPageStr)
                    } else {
                        nav.push('<li><a href="javascript:load(1);">01</a></li>');
                        nav.push('<li>...</li>');
                        for(var i=data.now-1;i<=data.now+1;i++){
                            var str = i+'';
                            if(str.length<=1) str='0'+str;
                            var now = '';
                            if(i===data.now) now='class="active"';
                            nav.push('<li '+now+'><a href="javascript:load('+i+');">'+str+'</a></li>');
                        }
                        nav.push('<li>...</li>');
                        nav.push(lastPageStr)
                    }
                }
                $(navUl).html(nav.join(''));
                if(data.now < pageCount) navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
            });
        },2000);
    });
}
function getUrl(page){
    var query = {
        type:newsObj.type
    }
    return config.urlPrefix + '/news/listNews?query='+encodeURIComponent(JSON.stringify(query))+ '&pageIndex=' + (page-1) +'&pageSize=' + PAGE_SIZE;
}