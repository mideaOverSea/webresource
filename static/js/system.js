$(document).ready(function(){

    window.getScreenSize = function () {
        var iWinWidth = window.innerWidth || $(window).width();
        return iWinWidth >= 1200 ? 'lg' : iWinWidth >= 992 ? 'md' : iWinWidth >= 768 ? 'sm' : 'xs';
    };
    var getScreen = getScreenSize();


    var bgChange = function () {
        var cWidth = document.documentElement.clientWidth;
        if (cWidth > 768) {
            $('*[data-pc-img]').each(function () {
                $(this).css('background-image', 'url('+$(this).attr('data-pc-img')+')');
            })

        } else {
            $('*[data-mb-img]').each(function () {
                $(this).css('background-image', 'url('+$(this).attr('data-mb-img')+')');
        })
    }
    }
    bgChange();



    $(".J-system-playBtn").click(function(){
        $(".J-detailsVideo").stop().show();
        var $url;
        $url=$(this).attr("data-url");
        systemplayVideo($url);
    });

    $(".J-detailsVideo .J-closeV").click(function(){
        $(".J-detailsVideo").stop().fadeOut();
        $(".J-detailsVideo #video").empty();
    });

    function systemplayVideo(url){
        jwplayer('video').setup({
            flashplayer: "../js/video/jwplayer.flash.swf",//调用播放器
            file: url,
            play:true,
            // image: src,
            width:'100%',
            height:'100%',
            events:{
                onReady:function(){
                    this.play();
                }
            }
        });
    };



    function systemSolution(){

        var $solutionBox = $(".fiber-system-solution"),
            $solutionItem = $solutionBox.find(".J-solution-item"),
            $videoButton = $solutionItem.find(".video-button"),
            $fiberUl = $solutionBox.find(".fiber-ul"),
            $fiberLi = $fiberUl.find("li"),
            $solutionText = $(".solution-txt"),
            $solutionAdd = $(".solution-icon"),
            $solutionTitle = $(".solution-tit"),
            $solutionTit = $(".showTxt"),
            $close = $(".closeFiber"),
            $sHeight = 55,
            $sWidth = 55,
            $boxWidth = $(window).width(),
            $boxHeight = $(".J-solution-list").height(),
            $itemLen = $(".J-solution-list").find(".J-solution-item").length,
            $itemLength = $solutionItem.length,
            $maxWidth = $boxWidth - $sWidth * ($itemLength - 1),
            $maxHeight = $boxHeight - ($sHeight * ($itemLength - 1));

        $(".J-solution-list").each(function(){
            if(getScreen === 'xs'){
                //mobile
                $(".J-solution-item").css("height",100 / $itemLen +'%');
            }else if(getScreen === 'sm'){
                //pad
            }else{
                //pc
                $(".J-solution-item").css("width",100 / $itemLen +'%');
            }
        });

        //观看视频
        var video = '<div id="videoMobile" style="position: fixed;top: 0;left: 0;z-index: 99999;background: rgba(0,0,0,0.6);width: 100%;height: 100%;display: block;">'+
						'<div style="position: relative;width: 94%;height: auto;top: 50%;left: 50%;-webkit-transform: translate(-50%,-50%);transform: translate(-50%,-50%);background: #000;" class="video-wrap">'+
							'<div style="position: absolute;top: -31px!important;right: 0!important;width: 31px;height: 31px;opacity: 1!important;" class="close">'+
								'<img style="display:block;width:70%;margin: 0 auto;" src="static/img/closeAi.png" alt=""/>'+
							'</div>'+
							'<video controls="controls" loop="loop" muted="true" autoplay="autoplay" style="width: 100%;height: auto;" src=""></video>'+
						'</div>'+
					'</div>';
        var pauseFlag = true;
        $videoButton.click(function(){
            $('body').append(video);
            var videoBox = $("#videoMobile"),
                videoSrc = videoBox.find('video');
            videoSrc.attr('src', $(this).attr('data-mobile-video'));
            console.log($(this).attr('data-mobile-video'));
            videoBox.fadeIn(200);
            pauseFlag = true;
        });
        $(document).on('click','.close',function(){
            $(document).find("#videoMobile").fadeOut(200);
            setTimeout(function(){
                $(document).find("#videoMobile").remove()
            },500)
        })

        $solutionItem.click(function(){
            var $that = $(this),
                $index = $that.index(),
                $dataPc = $fiberLi.eq($index).attr('data-pc'),
                $dataVideo = $fiberLi.eq($index).attr('data-video'),
                $dataIpad = $fiberLi.eq($index).attr('data-ipad'),
                $dataMb = $fiberLi.eq($index).attr('data-mobile');

            if(getScreen === 'xs'){
                //mobile
                if(!$(this).hasClass('active')){
                    $(this).height($maxHeight);
                    $(this).siblings().height($sHeight);
                    $(this).addClass("active").siblings().removeClass("active");
                    $that.find($solutionText).addClass("active");
                    $that.siblings().find($solutionText).removeClass("active");
                    $that.find($solutionAdd).fadeOut(100);
                    $that.siblings().find($solutionAdd).fadeOut(100);
                    $that.find($solutionTitle).fadeIn(100);
                    $that.siblings().find($solutionTitle).fadeOut(100);
                    $that.find($solutionTit).fadeOut(100);
                    $that.siblings().find($solutionTit).fadeIn(100);
                    $that.find($close).fadeIn(100).addClass("active");
                    $that.siblings().find($close).fadeOut(100).removeClass("active");

                    $fiberLi.removeClass('active').eq($index).css({
                        'background-image': 'url(' + $dataMb + ')',
                    }).addClass('active');

                    //remove视频
                    $fiberLi.find('video').remove();

                }else{
                    $that.find($solutionText).removeClass("active");
                };
            }else if(getScreen === 'sm'){
                //pad
                if(!$(this).hasClass('active')){
                    $(this).width($maxWidth);
                    $(this).siblings().show().width($sWidth);
                    $(this).addClass("active").siblings().removeClass("active");
                    $that.find($solutionText).addClass("active");
                    $that.siblings().find($solutionText).removeClass("active");
                    $that.find($solutionAdd).fadeOut(100);
                    $that.siblings().find($solutionAdd).fadeOut(100);
                    $that.find($solutionTitle).fadeIn(100);
                    $that.siblings().find($solutionTitle).fadeOut(100);
                    $that.find($solutionTit).fadeOut(100);
                    $that.siblings().find($solutionTit).fadeIn(100);
                    $that.find($close).fadeIn(100).addClass("active");
                    $that.siblings().find($close).fadeOut(100).removeClass("active");

                    $fiberLi.removeClass('active').eq($index).css({
                        'background-image': 'url(' + $dataPc + ')',
                    }).addClass('active');

                    if ($dataVideo !== '') {
                        //添加视频
                        $fiberLi.eq($index).html('<video autoplay="autoplay" muted="true" loop="loop" src="' + $dataVideo + '"></video>');
                    } else {
                        //remove视频
                        $fiberLi.find('video').remove();
                    }
                }else{
                    $that.find($solutionText).removeClass("active");
                };
            }else{
                //pc
                if(!$(this).hasClass('active')){
                    $(this).width($maxWidth);
                    $(this).siblings().show().width($sWidth);
                    $(this).addClass("active").siblings().removeClass("active");
                    $that.find($solutionText).addClass("active");
                    $that.siblings().find($solutionText).removeClass("active");
                    $that.find($solutionAdd).fadeOut(100);
                    $that.siblings().find($solutionAdd).fadeOut(100);
                    $that.find($solutionTitle).fadeIn(100);
                    $that.siblings().find($solutionTitle).fadeOut(100);
                    $that.find($solutionTit).fadeOut(100);
                    $that.siblings().find($solutionTit).fadeIn(100);
                    $that.find($close).fadeIn(100).addClass("active");
                    $that.siblings().find($close).fadeOut(100).removeClass("active");

                    $fiberLi.removeClass('active').eq($index).css({
                        'background-image': 'url(' + $dataPc + ')',
                    }).addClass('active');

                    if ($dataVideo !== '') {
                        //添加视频
                        $fiberLi.eq($index).html('<video autoplay="autoplay" muted="true" loop="loop" src="' + $dataVideo + '"></video>');
                    } else {
                        //remove视频
                        $fiberLi.find('video').remove();
                    }
                }else{
                    $that.find($solutionText).removeClass("active");
                };
            }


        })
        $close.click(function(){
            if(getScreen === 'xs'){
                //mobile
                $solutionItem.css("height",100 / $itemLength +'%');
            }else if(getScreen === 'sm'){
                //pad
            }else{
                //pc
                $solutionItem.css("width",100 / $itemLength +'%');
            }
            $solutionText.fadeIn(100);
            $solutionAdd.fadeIn(100);
            $solutionTitle.fadeIn(100);
            $solutionTit.fadeOut(100);
            $close.fadeOut(100).removeClass("active");
            setTimeout(function(){
                $solutionItem.removeClass("active");
                $fiberLi.removeClass("active");
                $fiberLi.find('video').remove();
            },50)
        })
        $(window).resize(function () {
            if (getScreenSize() !== 'xs') {
                var uWidth = $(window).width(),
                    maxWidth = uWidth - $sHeight * ($itemLength - 1);

                for (var i = 0; i < $solutionItem.length; i++) {
                    if ($solutionItem.eq(i).hasClass('active')) {
                        $solutionItem.eq(i).css('width', maxWidth);
                    }
                }
            }
        });
    }
    systemSolution();




})