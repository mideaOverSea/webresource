/*
*	BangFlow v1.0.0
*/

//编译等操作依赖包
var gulp 		= require('gulp');
var sass        = require('gulp-sass');             //编译sass、scss
var less        = require('gulp-less');             //编译less
var jade        = require('gulp-jade');             //编译jade
var pug         = require('gulp-pug');              //编译pug
var concat      = require('gulp-concat');           //合并
var postcss     = require('gulp-postcss');			//样式后处理器
var autoprefixer= require('autoprefixer');          //css前缀
var browserSync = require('browser-sync').create(); //浏览器同步
var reload		= browserSync.reload;
//gulp插件依赖包
var htmlreplace = require('gulp-html-replace');		//html页面引用替换
var cssmin 		= require('gulp-minify-css');       //css压缩
var imagemin 	= require('gulp-imagemin');         //图片压缩
var uglify = require('gulp-uglify');                //js压缩
var tinypng		= require('gulp-tinypng-nokey')		//模拟上传至tinypng进行压缩，速度较慢
var notify 		= require('gulp-notify');           //显示报错信息
var changed 	= require('gulp-changed');          //监听文件改变
var sourcemaps	= require('gulp-sourcemaps')		//编译同时输出sourcemap文件方便chrome下调整修改
var gulpif = require('gulp-if');
var debug	= require('gulp-debug')
var data = require('gulp-data');
var replace = require('gulp-replace');
var del = require('del');
var vinylPaths = require('vinyl-paths');
var fs = require('fs');
var path = require('path');
var zip = require('gulp-zip');                      //zip压缩包

//本地开发
gulp.task('default',['template','styles','script','images','video','vendor','fileWatch']);

//使用browsersync插件静态代理
gulp.task('localServer',function(){
    browserSync.init({server: './dist/'});
})

//文件监视
gulp.task('fileWatch',function(){

    //模版监视
    gulp.watch("template/*.pug", function(event){
        jadeEvent(event.path);
    });

    //模版监视
    gulp.watch("template/module/*.pug", function(event){
        jadeEvent("template/**/*.pug");
    });

    //scss文件监视
    gulp.watch("static/css/**/*.scss", function(event){
        scssEvent(event.path);
    });

    //js文件监视
    gulp.watch("static/js/**/*.js", function(event){
        scriptEvent(event.path);
    });

    //css文件监视
    gulp.watch("static/css/**/*.css", function(event){
        cssEvent(event.path);
    });

    //图片监视
    gulp.watch("static/img/**/*.*", function(event){
        imagesEvent(event.path);
    });

    gulp.watch("static/video/**/*.*", function(event){
        videoEvent(event.path);
    });

    //第三方插件
    gulp.watch("vendor/**/*.*", function(event){
        vendorEvent(event.path);
    });

});

//编译模板task
gulp.task('template',function(){

    jadeEvent("template/**/*.pug");

})

//编译样式task
gulp.task('styles',function(){
    scssEvent("static/css/**/*.scss");
})


//脚本task
gulp.task('script',function(){
    scriptEvent("static/js/**/*.js")
})

//css样式 task
gulp.task('css',function(){
    cssEvent("static/css/**/*.css")
})

//图片task
gulp.task('images',function(){

    imagesEvent("static/img/**/*.*")

})

//视频
gulp.task('video',function(){

    videoEvent("static/video/**/*.*")

})

//移动第三方插件
gulp.task('vendor',function(){

    vendorEvent("vendor/**/*.*")

})

//清空Out
gulp.task('clean', function (cb) {
    del(['dist/**/*.*'], cb);
});

//打包成zip
gulp.task('zip', function() {

    return gulp.src('dist/**/*.*')
        .pipe(zip('html.zip'))
        .pipe(gulp.dest(''));
});

/**
 * 移动第三方插件
 * @param src
 */
function vendorEvent(src){

    // gulp.src([src],{ base: 'vendor' })
    // .pipe(cssmin())
    //     .pipe(debug({title: '移动文件:'}))
    //     .pipe(gulp.dest('./dist/vendor/'))


    minVendorScript()
    minVendorStyle()
    // moveLaydate()
    minVendorFont()
    //minVendorImage()
    //minVendorOther()
    minVendorVideo()
}

/**
 * 压缩移动第三方插件-脚本
 */
function minVendorScript(){

    var scriptArr = [
        'vendor/js/jquery.min.js',
        'vendor/js/wow.js',
        'vendor/js/jHsDate.js',
        'vendor/js/Popt.js',
        'vendor/js/city.json.js',
        'vendor/js/jwplayer.js',
        'vendor/js/jwplayer.html5.js',
        'vendor/js/Scrollbar.min.js'
    ]

    gulp.src(scriptArr)
        .pipe(concat('vendor.js'))              //合并完成的文件
        .pipe(uglify())                         //压缩脚本
        .pipe(debug({title: '移动第三方-脚本:'}))
        .pipe(gulp.dest('./dist/vendor/js/'))

}

/**
 * 压缩移动第三方插件-脚本
 */
function moveLaydate(){

    gulp.src('vendor/laydate/**/*.*')

        .pipe(debug({title: '移动第三方-脚本:'}))
        .pipe(gulp.dest('./dist/vendor/laydate/'))

}

/**
 * 压缩移动第三方插件-样式
 */
function minVendorStyle(){

    var styleArr = [
        'vendor/css/swiper-3.4.0.min.css',
        'vendor/css/swiper-2.7.6.min.css',
    ]
    gulp.src(styleArr)
        // .pipe(concat('vendor.css'))
        // .pipe(cssmin())
        .pipe(debug({title: '移动第三方-样式:'}))
        .pipe(gulp.dest('./dist/static/css/'))

}

/**
 * 压缩移动第三方插件-字体
 */
function minVendorFont(){

    gulp.src('vendor/font/**/*.*')
        .pipe(debug({title: '移动第三方-字体:'}))
        .pipe(gulp.dest('./dist/static/font/'))

}

/**
 * 压缩移动第三方插件-图片
 */
function minVendorImage(){

    gulp.src('vendor/img/**/*.*')
        .pipe(imagemin())
        .pipe(debug({title: '移动第三方-图片:'}))
        .pipe(gulp.dest('./dist/vendor/img/'))

}

/**
 * 压缩移动第三方插件-视频
 */
function minVendorVideo(){

    gulp.src('vendor/video/**/*.*')
        .pipe(debug({title: '移动第三方-视频:'}))
        .pipe(gulp.dest('./dist/static/video/'))

}

/**
 * 压缩移动第三方插件-其他
 */
function minVendorOther(){

    gulp.src('vendor/other/**/*.*')
        .pipe(debug({title: '移动第三方-其他:'}))
        .pipe(gulp.dest('./dist/vendor/other/'))

}

/**
 * 编译jade模版
 * @param src 要处理的.jade文件地址
 */
function jadeEvent(src){

    gulp.src([src, '!template/module/*.pug'] ,{ base: 'template' })
        .pipe(pug({pretty:true})
            .on('error', notify.onError(function(){
                return 'jade编译失败, ${error}';
            }))
        )
        .pipe(debug({title: '编译jade:'}))
        .pipe(gulp.dest('./dist/'))


}

/**
 * json监听
 * @param src
 */
function jsonEvent(src){

    var jadeFile = src.replace('.json', '.pug')
    jsonJade2html(jadeFile)

}

/**
 * 根据jade文件编译
 * @param src
 */
function jade2html(src){
    gulp.src([src, '!template/module/*.pug'],{ base: 'template' })

        .pipe(pug({pretty:true})
            .on('error', notify.onError(function(){
                return 'jade编译失败, ${error}';
            }))
        )
        .pipe(gulp.dest('out'))
        .pipe(debug({title: '编译jade:'}))
}

/**
 * 根据json文件编译jade文件
 * @param src
 */
function jsonJade2html(src){

    gulp.src([src, '!template/module/*.pug'],{ base: 'template' })

        .pipe(gulpif(fsExistsSync, data(function(file) {
            jsonFile = file.path.replace('.pug', '.json')
            return JSON.parse(fs.readFileSync(jsonFile));
        })))

        .pipe(pug({pretty:true})
            .on('error', notify.onError(function(){
                return 'jade编译失败, ${error}';
            }))
        )
        .pipe(gulp.dest('./dist/'))
        .pipe(debug({title: '编译jade:'}))
}

/**
 * 编译scss样式
 * @param src 要处理的.scss文件地址
 */
function scssEvent(src){
    /*
    gulp.src(src,{ base: 'static/css/' })
        //.pipe(sass({outputStyle: 'compact'}).on('error', notify.onError( (function(){
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', notify.onError( (function(){
            return 'scss 编译错误, ${error}';
        }) )))
        .pipe(postcss([autoprefixer({browsers:'>0%'})]))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest("./dist/static/css/"))
        .pipe(debug({title: '编译scss:'}))
        .pipe(browserSync.reload({stream: true}))
    */
    gulp.src(['static/css/util.scss','static/css/reset.scss'],{ base: 'static/css/' })
        .pipe(sourcemaps.init())
        //.pipe(sass({outputStyle: 'compact'}).on('error', notify.onError( (function(){
        .pipe(sass({outputStyle: 'compressed'}).on('error', notify.onError( (function(){
            return 'scss 编译错误, ${error}';
        }) )))
        .pipe(postcss([autoprefixer({browsers:'>0%'})]))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest("./dist/static/css/"))
        .pipe(debug({title: '编译scss:'}))
        .pipe(browserSync.reload({stream: true}))

    gulp.src('static/old/*.*',{ base: 'static/css/' })
        .pipe(concat('old.css'))
        .pipe(gulp.dest("./dist/static/css/"))

}

/**
 * css样式
 * @param src
 */
function cssEvent(src){

    gulp.src('static/css/**/*.*')
    // .pipe(concat('vendor.css'))
        .pipe(cssmin())
        .pipe(debug({title: '移动文件:'}))
        .pipe(gulp.dest('./dist/static/css/'))

}

/**
 * 脚本操作
 * @param src
 */
function scriptEvent(src){

    gulp.src('static/js/**/*.*')
        // .pipe(concat('vendor.js'))
        // .pipe(uglify())
        .pipe(debug({title: '移动文件:'}))
        .pipe(gulp.dest('./dist/static/js/'))

}

/**
 * 图片操作
 * @param src
 */
function imagesEvent(src){
    gulp.src(src)
        .pipe(debug({title: '移动文件:'}))
        .pipe(gulp.dest('./dist/static/img/'))
}

function videoEvent(src){
    gulp.src(src)
        .pipe(debug({title: '移动文件:'}))
        .pipe(gulp.dest('./dist/static/video/'))
}




function fsExistsSync(file) {
    jsonFile = file.path.replace('.pug', '.json')
    try{
        fs.accessSync(jsonFile,fs.F_OK);
    }catch(e){
        return false;
    }
    return true;
}
