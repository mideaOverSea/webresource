$(document).ready(function() {
    if($(".J-cf-allpro").length > 0){
        load(1);
    }
    $(".J-cf-nav .swiper-slide").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
    })
});
var $first=true;
function scrollTo(element,speed) {
    if(!speed)speed = 100;
    if(!element) $("html,body").animate({scrollTop:0},speed);
    else{
        if(element.length>0){
            $("html,body").animate({scrollTop:$(element).offset().top-101},speed);
        }
    }
}
function load(page) {
    var $url = getUrl(page);
    $('#loading').show();
    if(!$first)scrollTo('#toper',500);
    $first=false;
    $('.J-cf-allpro').each(function () {
        var $this = this;
        setTimeout(function () {
            $($this).html('');
        },260);
        setTimeout(function () {
            $.getJSON($url, function (data) {
                console.log(data, $this);
                $($this).append($("<h3 class=\"sub-tit\"></h3>").text(data.title.toString()));
                var $container = $("<div class=\"sub-container\"></div>");
                $($this).append($container);
                var $list = $("<div class=\"cf-pro-list clear\"></div>");
                $($container).append($list);
                var $code = [];
                for (var i = 0; i < data.rows.length; i++) {
                    var $row = data.rows[i];
                    $code.push('<div class="cf-pro-item">' +
                        '<a href="' + $row.href + '">' +
                        '<img src="' + $row.src + '">' +
                        '<div class="proName">'+
                        '<span class="nm">' + $row.title + '</span>' +
                        '<span class="xh">' + $row.type + '</span></div></a></div>');
                }
                $($list).html($code.join(''));
                var $nav = [];
                var $navContainer = $("<div class=\"m-page-number\"></div>");
                var $navUl = $('<ul></ul>');
                $($this).append($navContainer);
                $($navContainer).append($navUl);
                $nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
                if(data.now<5){
                    for(var i=1;i<=5;i++){
                        var $str = '0'+i;
                        var $now = '';
                        if(i===data.now) $now='class='+_ACTIVE_+'';
                        $nav.push('<li '+$now+'><a href="javascript:load('+i+');">'+$str+'</a></li>');
                    }
                    $nav.push('<li>...</li>');
                }else if(data.now> data.total-5){
                    $nav.push('<li><a href="javascript:load(1);">01</a></li>');
                    $nav.push('<li>...</li>');
                    for(var i=data.total-5;i<=data.total;i++){
                        var $str = i+'';
                        if($str.length<=1) $str='0'+$str;
                        var $now = '';
                        if(i===data.now) $now='class='+_ACTIVE_+'';
                        $nav.push('<li '+$now+'><a href="javascript:load('+i+');">'+$str+'</a></li>');
                    }
                }else{
                    $nav.push('<li><a href="javascript:load(1);">01</a></li>');
                    $nav.push('<li>...</li>');
                    for(var i=data.now-1;i<data.now+4;i++){
                        var $str = i+'';
                        if($str.length<=1) $str='0'+$str;
                        var $now = '';
                        if(i===data.now) $now='class='+_ACTIVE_+'';
                        $nav.push('<li '+$now+'><a href="javascript:load('+i+');">'+$str+'</a></li>');
                    }
                    $nav.push('<li>...</li>');
                }

                $($navUl).html($nav.join(''));
                if(data.now+4 < data.total) $navUl.append('<li><a href="javascript:load('+(data.total)+');">'+(data.total)+'</a></li>');
                $navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
            });
            console.log(this);
            $('#loading').hide();
        }, 2000)
    });
}
function getUrl(page){
    return "static/json/data" + page + ".json";
}