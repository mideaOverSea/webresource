var arr;
$(document).ready(function () {
    var _h2 = $(".m-header").height();
    $(".compare-box").css("top",_h2+"px");
    $(window).resize(function(){
        var _h2 = $(".m-header").height();
        $(".compare-box").css("top",_h2+"px");
    });

  var _h = $(".compare-top-pro").height();
  $(".compare-box").height(_h);

  if($(".compare-top-pro").length > 0){
      $(window).scroll(function(){
          var _point = $(".compare-parameter").offset().top;
          if($(window).scrollTop() > _point){
              $(".compare-box").addClass("fixed");
              $(".compare-parameter").css("margin-top",_h);
          }else{
              $(".compare-box").removeClass("fixed");
              $(".compare-parameter").css("margin-top","0");
          }
      })
  }


    var supportsPointerEvents = (function(){
      var dummy = document.createElement('_');
      if(!('pointerEvents' in dummy.style)) return false;
      dummy.style.pointerEvents = 'auto';
      dummy.style.pointerEvents = 'x';
      document.body.appendChild(dummy);
      var r = getComputedStyle(dummy).pointerEvents === 'auto';
      document.body.removeChild(dummy);
      return r;
    })();


  /*产品列表页对比*/
  $(document).on("click", ".J-fun-sc", function () {
    if (!$(this).hasClass(_ACTIVE_)) {
      $(this).addClass(_ACTIVE_);
      $(this).find("span").text("取消收藏");
    } else {
      $(this).removeClass(_ACTIVE_);
      $(this).find("span").text("收藏");
    }
  })
  /** bin */
  // window.location.reload=function(){
  //   console.log('reload');
  //   localStorage.removeItem('compare');
  // };
  //localStorage.removeItem('compare');
  if (window.localStorage) {
    var v = localStorage.getItem('compare');
    console.log('compare', v);
    if (!v) {
      arr = [];
    } else {
        arr = JSON.parse(v);
        if (arr.length > 0) {
          if(window.location.href.indexOf('compared')>-1){
            window.location.href=window.location.href.replace('#compared','#ready');
            $(".pro-compare").show();
            $(".pro-compare-list ul").each(function () {
              for (var i = 0; i < arr.length; i++) {
                var obj = arr[i];
                var _li_html = '<li data-val="' + obj.id + '">' +
                  '<a class="closeLi" href="javascript:;"></a>' +
                  '<div class="pro-compare-img"><a href="javascript:;"><img src="' + obj.img + '" alt=""></a></div>' +
                  '<div class="pro-compare-txt">' +
                  '<a href="javascript:;">' +
                  '<div class="pro-c-name">' + obj.name + '</div>' +
                  '<div class="pro-c-model">' + obj.model + '</div>' +
                  '</a>' +
                  '</div>' +
                  '</li>';
                console.log('obj', obj);
                $(this).append(_li_html);
              }
            });
          }else{
            //localStorage.removeItem('compare');
            arr=[];
          }
          $(".c-top-r-list").each(function () {
            for (var i = 0; i < arr.length; i++) {
              var obj = arr[i];
              var html1 = '<div class="c-top-r-item c-top-r-item1"  data-val="' + obj.id + '">' +
                '<div class="c-item-row">' +
                '<div class="J-close-c-item close-c-item"  data-val="' + obj.id + '"></div><a href="javascript:;"><img src="' + obj.img + '">' +
                '<div class="c-item-txt">' +
                '<div class="c-top-pro-name">' + obj.name + '</div>' +
                '<div class="c-top-pro-model">' + obj.model + '</div>' +
                '</div></a>' +
                '</div>' +
                '</div>';
                $(this).prepend(html1);
            }
          });
        }
      
    }
    if(arr.length===0){
      $('.parameter-item').hide();
    }
    $(".parameter-item dl").each(function () {
      $(this).find("dd").each(function(index){
        if(index>=arr.length){
          $(this).hide();
        }
      });
    });
    $('#num').html(arr.length);
  } else {
    console.log(window.sessionStorage);
  }
  console.log('compare', arr);
  //console.log(window.sessionStorage,window.localStorage);
  /** end */
  $(document).on("click", ".J-fun-db", function () {
    var _this = $(this);
    var _pro_id = $(this).parents(".cf-pro-item").attr("id");
    var _pro_img = $(this).attr("data-img");
    var _pro_name = $(this).attr("data-name");
    var _pro_model = $(this).attr("data-model");

    var _li_html = '<li data-val="' + _pro_id + '">' +
      '<a class="closeLi" href="javascript:;"></a>' +
      '<div class="pro-compare-img"><a href="javascript:;"><img src="' + _pro_img + '" alt=""></a></div>' +
      '<div class="pro-compare-txt">' +
      '<a href="javascript:;">' +
      '<div class="pro-c-name">' + _pro_name + '</div>' +
      '<div class="pro-c-model">' + _pro_model + '</div>' +
      '</a>' +
      '</div>' +
      '</li>';
    if (!_this.hasClass(_ACTIVE_)) {
      _this.addClass(_ACTIVE_);
      _this.find("span").text("取消对比");
      $(".cf-pro-list").each(function(){
        var _pro_len = $(this).find(".active").length;
          if($(window).width() > 768){
            if(_pro_len >= 4){
                $(document).find(".J-fun-db").addClass("disabled").attr("disabled",true).css("pointer-events","none");
                $(document).find(".J-fun-db.active").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
                return false;
            }else{
                $(document).find(".J-fun-db").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
                $(document).find(".J-fun-db.active").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
            }
          }else{
              if(_pro_len >= 2){
                  $(document).find(".J-fun-db").addClass("disabled").attr("disabled",true).css("pointer-events","none");
                  $(document).find(".J-fun-db.active").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
                  return false;
              }else{
                  $(document).find(".J-fun-db").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
                  $(document).find(".J-fun-db.active").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
              }
          }
      });
      $(".pro-compare-list ul").each(function () {
        var _LI_len = $(this).find("li").length;
        if($(window).width() > 768){
          $(".pro-compare-con .pro-compare-tit span").text("（*最多可添加4款产品进行对比）");
          if (_LI_len >= 4) {
              alert("您最多能添加4个产品！");
              _this.removeClass(_ACTIVE_);
              _this.find("span").text("加入对比");
              return false;
          } else {
              /*** bin */
              // if (window.localStorage) {
              //     var obj = { id: _pro_id, img: _pro_img, name: _pro_name, model: _pro_model };
              //     arr.push(obj);
              //     localStorage.setItem('compare', JSON.stringify(arr));
              // }
              var obj = { id: _pro_id, img: _pro_img, name: _pro_name, model: _pro_model };
              add(obj);
              /*** end */
              $(".pro-compare").show();
              $(this).append(_li_html);
          }
        }else{
          $(".pro-compare-con .pro-compare-tit span").text("（*最多可添加2款产品进行对比）");
          if (_LI_len >= 2) {
              alert("您最多能添加2个产品！");
              _this.removeClass(_ACTIVE_);
              _this.find("span").text("加入对比");
              return false;
          } else {
              /*** bin */
              // if (window.localStorage) {
              //     var obj = { id: _pro_id, img: _pro_img, name: _pro_name, model: _pro_model };
              //     arr.push(obj);
              //     localStorage.setItem('compare', JSON.stringify(arr));
              // }
              var obj = { id: _pro_id, img: _pro_img, name: _pro_name, model: _pro_model };
              add(obj);
              /*** end */
              $(".pro-compare").show();
              $(this).append(_li_html);
              console.log(_LI_len)
          }
        }
      })
    } else {
      _this.removeClass(_ACTIVE_);
      _this.find("span").text("加入对比");
      $(document).find(".J-fun-db").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
      $(document).find(".J-fun-db.active").removeClass("disabled").attr("disabled",false).css("pointer-events","inherit");
      $(".pro-compare-list ul").each(function () {
        var _LI_len2 = $(this).find("li").length;
        $(this).find("li[data-val=" + _pro_id + "]").remove();
        // var arr1=[];
        // for (var i = 0; i < arr.length; i++) {
        //   var obj=arr[i];
        //   console.log(obj.id,'???',_pro_id);
        //   //console.log('this.dataset.val',this.dataset.val,obj);
        //   if(_pro_id !== obj.id){
        //      arr1.push(obj);
        //   }
        // }
        // localStorage.setItem('compare', JSON.stringify(arr1));
        // arr=arr1;
        remove(_pro_id);
        console.log(_LI_len2)
        if (_LI_len2 <= 1) {
          $(".pro-compare").hide();
        }
      })
    }
  })
  $(".pro-compare-tit").find(".slide-compare").click(function () {
    if (!$(this).hasClass(_ACTIVE_)) {
      $(this).addClass(_ACTIVE_);
      $(this).text("收起");
      $(".pro-compare-list").slideDown(150);
    } else {
      $(this).removeClass(_ACTIVE_);
      $(this).text("展开");
      $(".pro-compare-list").slideUp(150);
    }
  })
  $(document).on("click", ".closeLi", function () {
    var _li_val = $(this).parents("li").attr("data-val");
    /** bin */
    // if (window.localStorage) {
    //   var arr1 = [];
    //   for (var i = 0; i < arr.length; i++) {
    //     var obj = arr[i];
    //     if (obj.id != _li_val) {
    //       arr1.push(obj);
    //     }
    //   }
    //   localStorage.setItem('compare', JSON.stringify(arr1));
    // }
    remove(_li_val);
    /** end */
    console.log('_li_val', _li_val);
    $(document).find("#" + _li_val + "").find(".J-fun-db").removeClass(_ACTIVE_);
    $(document).find("#" + _li_val + "").find(".J-fun-db").children("span").text("加入对比");
    $(this).parents("li").remove();
    $(".pro-compare-list ul").each(function () {
      var _LI_len = $(this).find("li").length;
      if (_LI_len <= 0) {
        $(".pro-compare").hide();
      }
    })
  })
  /*开始对比*/
  $(".pro-compare-fun").find(".J-start-c").click(function () {
    var _this = $(this);
    $(".pro-compare-list ul").each(function () {
      var _LI_len = $(this).find("li").length;
      if (_LI_len < 2) {
        alert("您必须至少选择两件产品才能进行对比。")
        return false;
      } else {
        _this.attr("href", _this.attr("data-backurl"));
        return true;
      }
    })
  })
  /*清空对比*/
  $(".pro-compare-fun").find(".J-empty-c").click(function () {
    /** bin */
    if (window.localStorage) {
      localStorage.removeItem('compare');
    }
    /** end */
    $(document).find(".J-fun-db").removeClass(_ACTIVE_);
    $(document).find(".J-fun-db").find("span").text("加入对比");
    $(".pro-compare").hide();
    $(".pro-compare-list ul").empty();
  });


  /*产品对比页面-各类参数显示隐藏*/
  $(".parameter-item").find(".J-pSlideBtn").click(function () {
    if (!$(this).parents(".parameter-item").hasClass(_ACTIVE_)) {
      $(this).parents(".parameter-item").addClass(_ACTIVE_);
      $(this).parents(".parameter-item").find(".parameter-item-data").slideDown(150);
    } else {
      $(this).parents(".parameter-item").removeClass(_ACTIVE_);
      $(this).parents(".parameter-item").find(".parameter-item-data").slideUp(150);
    }
  })
  /*产品对比页面-清空所有产品*/
  var _num;
  if($(window).width() > 768){
      _num = 4;
  }else{
      _num = 2;
  }
  var _addHtml = '<div class="c-item-row addBox">' +
    '<a class="add" href="product-kongtiao.html#compared">' +
    '<img src="static/img/cf-pro-zwImg.png" alt="">' +
    '<div class="c-item-add-link">' +
    '<span>+</span><p>添加一款产品</p>' +
    '</div>' +
    '<div class="c-item-add-ts">' +
    '<p>“最多可添加'+_num+'款产品”</p>' +
    '</div>' +
    '</a>' +
    '</div>';
  $(".J-clear").click(function () {
    $(".c-top-r-item").empty();
    $(".c-top-r-item").html(_addHtml);
    //$(".c-t-num").find("span").text("0");
    $(".compare-parameter").empty();
  })
  $(".c-top-r-list").each(function () {
    var _l = $(this).find(".addBox").length;
    var _n = 4 - _l;
    //$(".c-t-num").find("span").text(_n);
  });
  $(".J-close-c-item").click(function () {
    var _cItemIndex = $(this).parents(".c-top-r-item").index();
    /** bin */
    // var arr1=[];
    // //$('.c-top-r-item1').each(function(el){
      
    //   for (var i = 0; i < arr.length; i++) {
    //     var obj=arr[i];
    //     console.log('this.dataset.val',this,obj.id);
    //     if(this.dataset.val !== obj.id){
    //       arr1.push(obj);
    //     }
    //   }
      
    // //});
    // localStorage.setItem('compare', JSON.stringify(arr1));
    //   arr = arr1;
    //   console.log(arr);
    remove(this.dataset.val);
    /** end */
    $(this).parents(".c-top-r-item").remove();
    $(".c-top-r-list").append("<div class='c-top-r-item'>" + _addHtml + "</div>");
    $(".parameter-item dl").each(function () {
      $(this).find("dd").each(function(index){
        if(index==_cItemIndex) $(this).hide();
      });
    });
    $(".c-top-r-list").each(function () {
      var _l = $(this).find(".addBox").length;
      var _n = 4 - _l;
      //$(".c-t-num").find("span").text(_n);
      if (_l === 4) {
        $(".c-top-r-item").empty();
        $(".c-top-r-item").html(_addHtml);
        //$(".c-t-num").find("span").text("0");
        $(".compare-parameter").empty();
      }
    })
  });
    $(".c-top-r-list").each(function () {
        var _l2 = $(this).find(".c-top-r-item").length;
        console.log(_l2);
        if($(window).width() > 768){
          $(document).find(".c-item-add-ts").children("p").text("“最多可添加4款产品”");
          if(_l2 > 4){
              $(this).find(".c-top-r-item:last-child").hide();
          }
        }else{
          $(document).find(".c-item-add-ts").children("p").text("“最多可添加2款产品”");
          if(_l2 > 2){
              $(this).find(".c-top-r-item:last-child").hide();
          }
        }

    })

  /*产品对比页面-高亮显示不同项*/
  $(".J-fun-highlight").click(function () {
    if (!$(this).hasClass("active")) {
      $(this).addClass("active");
      highlight();
    } else {
      $(this).removeClass("active");
      $('.table_compareCon').each(function () {
        var el = this;
        el.classList.remove('active');
      })
    }
  });
  function highlight() {
    $('.table_compareCon').each(function () {
      var el = this;
      el.classList.remove('active');
      // el.classList.remove('hide');
      var equal = false, values = [];
      $(el).find('dd').each(function (i) {
        values[i] = this.innerHTML.trim();
      });
      var temp = null;
      for (var j = 0; j < values.length; j++) {
        if (values[j] !== '&nbsp;' && values[j] !== '') {
          if (temp === null) {
            temp = values[j];
          } else {

            if (temp === values[j]) {
              equal = true;
            } else {
              equal = false;
            }
          }
        }
      }
      if (!equal) el.classList.add('active');
    });
  }

  /*产品对比页面-隐藏相同项*/
  $(".J-fun-hide").click(function () {
    if (!$(this).hasClass("active")) {
      $(this).addClass("active");
      hideIdentical()
    } else {
      $(this).removeClass("active");
      $('.table_compareCon').each(function () {
        var el = this;
        el.classList.remove('hide');
      })
    }
  });
  function hideIdentical() {
    $('.table_compareCon').each(function () {
      var el = this;
      // el.classList.remove('active');
      el.classList.remove('hide');
      var equal = false, values = [];
      $(el).find('dd').each(function (i) {
        values[i] = this.innerHTML.trim();
      });
      var temp = null;
      for (var j = 0; j < values.length; j++) {
        if (values[j] !== '&nbsp;' && values[j] !== '') {
          if (temp === null) {
            temp = values[j];
          } else {
            if (temp === values[j]) {
              equal = true;
            } else {
              equal = false;
            }
          }
        }
      }
      if (equal) el.classList.add('hide');
    });
  }

});
/** bin */
var compareFn = function () {
  setTimeout(function () {
    console.log('compareFn', arr);
    if (arr && arr.length > 0) {
      if (arr.length > 0) {
        // $(".pro-compare").show();
        // $(".pro-compare-list ul").each(function () {
        for (var i = 0; i < arr.length; i++) {
          var obj = arr[i];
          $(document).find("#" + obj.id + "").find(".J-fun-db").addClass(_ACTIVE_);
          $(document).find("#" + obj.id + "").find(".J-fun-db").children("span").text("取消对比");
        }
      }
    }
  }, 500);

}
/** end */
var add = function(obj){
  if (window.localStorage) {
    //var obj = { id: _pro_id, img: _pro_img, name: _pro_name, model: _pro_model };
    var flag=false;
    for(var i in arr){
      if(arr[i].id === obj.id){
        flag=true
      }
    }
    if(!flag){
      arr.push(obj);
      localStorage.setItem('compare', JSON.stringify(arr));
    }
    $('#num').html(arr.length);
  }
}
var remove = function(id){
  var arr1=[];
  for (var i = 0; i < arr.length; i++) {
    var obj=arr[i];
    //console.log(obj.id,'???',_pro_id);
    //console.log('this.dataset.val',this.dataset.val,obj);
    if(id !== obj.id){
       arr1.push(obj);
    }
  }
  arr = arr1;
  localStorage.setItem('compare', JSON.stringify(arr));
  console.log('arr',arr,arr.length);
  $('#num').html(arr.length);
}