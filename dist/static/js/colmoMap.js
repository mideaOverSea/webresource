$(function(){

    /**全局变量*/
    _ACTIVE_ = "active";


    if($(".J-store-wrap").length > 0){


        /**城市选择*/
        $(".J-store-ul").removeClass(_ACTIVE_);
        $(".J-store-select").click(function(){
            $(this).find(".J-store-ul").addClass(_ACTIVE_);
        })
        $(".J-store-select").bind('mouseleave',function(){
            $(this).find(".J-store-ul").removeClass(_ACTIVE_);
        })

        /**门店地图*/
        $(".J-store-list-ul #list li").each(function(){
            var $liNum = $(this).index();
            $(this).find("span").html($liNum + 1);
        });

        if($(window).width() < 1024){
            $(".J-store-wrap").removeClass(_ACTIVE_)
            $(".J-store-list-ul #list").on("click","a",function(){
                $(".J-store-wrap").addClass(_ACTIVE_);
            });
            $(".J-store-wrap .store-map .returnBtn").click(function(){
                $(".J-store-wrap").removeClass(_ACTIVE_);
            })
        }

        var $def={p:3,c:31,a:311};
        var $provinceData = [
            {
                id:1,
                name:'北京'
                , city:[
                    {
                        id:11,name:'北京市',
                        area:[
                            {id:111,name:'东城区'},
                            {id:112,name:'西城区'}
                        ]
                    }
                ]
            },
            {
                id:2,
                name:'河北省'
                , city:[
                    {
                        id:21,name:'石家庄',
                        area:[
                            {id:211,name:'桥西区'},
                            {id:212,name:'裕华区'}
                        ]
                    },
                    {
                        id:22,name:'邯郸',
                        area:[
                            {id:213,name:'桥西区'},
                            {id:214,name:'裕华区'}
                        ]
                    }
                ]
            },
            {
                id:3,
                name:'广东省'
                , city:[
                    {
                        id:31,name:'佛山市',
                        area:[
                            {id:311,name:'顺德区'},
                            {id:312,name:'禅城区'}
                        ]
                    },
                    {
                        id:32,name:'深圳市',
                        area:[
                            {id:313,name:'福田区'},
                            {id:314,name:'罗湖区'}
                        ]
                    }
                ]
            }
        ];
        var $data = [
            {id:1,province:1,city:11,area:111,type:1,pro:[1,2],name:'东城王府井大街店',info:'王府井apm',x:116.418664,y:39.922343}
            , {id:2,province:1,city:11,area:112,type:2,pro:[2,3],name:'西城区',info:'居然之家B馆5楼',x:116.4793,y:39.91699}
            , {id:3,province:2,city:21,area:211,type:1,pro:[1,3,7],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:111.1,y:25.915809}
            , {id:4,province:2,city:21,area:212,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115.1,y:32.915809}
            , {id:5,province:1,city:11,area:111,type:3,pro:[1,2],name:'东城王府井大街店',info:'王府井apm',x:116.428664,y:39.922343}
            , {id:6,province:1,city:11,area:112,type:2,pro:[2,3,4],name:'西城区',info:'居然之家B馆5楼',x:116.3893,y:39.91699}
            , {id:7,province:2,city:21,area:211,type:1,pro:[1,3],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:111.2,y:25.915809}
            , {id:8,province:2,city:21,area:212,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115.4,y:32.915809}
            , {id:9,province:1,city:11,area:111,type:1,pro:[1,2,6],name:'东城王府井大街店',info:'王府井apm',x:116.438664,y:39.922343}
            , {id:10,province:1,city:11,area:112,type:2,pro:[2,3,4],name:'西城区',info:'居然之家B馆5楼',x:116.3893,y:39.91699}
            , {id:11,province:2,city:21,area:211,type:3,pro:[1,3],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:111.5,y:25.916809}
            , {id:12,province:2,city:21,area:212,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115.4,y:32.925809}
            , {id:13,province:1,city:11,area:111,type:1,pro:[1,2,5,6],name:'东城王府井大街店',info:'王府井apm',x:116.428664,y:39.925343}
            , {id:14,province:1,city:11,area:112,type:2,pro:[2,3],name:'西城区',info:'居然之家B馆5楼',x:116.3703,y:39.92699}
            , {id:15,province:2,city:21,area:211,type:1,pro:[1,3],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:111.8,y:25.925809}
            , {id:16,province:2,city:21,area:212,type:2,pro:[1,2,3,5],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:114,y:32.935809}
            , {id:17,province:1,city:11,area:111,type:4,pro:[1,2],name:'东城王府井大街店',info:'王府井apm',x:117.418664,y:39.022343}
            , {id:18,province:1,city:11,area:112,type:2,pro:[2,3,6,7],name:'西城区',info:'居然之家B馆5楼',x:116.37,y:39.91099}
            , {id:19,province:2,city:21,area:211,type:1,pro:[1,3,5],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:101,y:25.9}
            , {id:20,province:2,city:21,area:212,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115.8,y:33.115809}
            , {id:21,province:1,city:11,area:111,type:4,pro:[1,2],name:'东城王府井大街店',info:'王府井apm',x:112.418664,y:38.022343}
            , {id:22,province:1,city:11,area:112,type:2,pro:[2,3,6],name:'西城区',info:'居然之家B馆5楼',x:116.1793,y:39.1699}
            , {id:23,province:2,city:21,area:211,type:1,pro:[1,3,6,7],name:'桥西区红旗大街店',info:'体验店1路77号居然之家B馆5楼',x:111.9,y:22.915809}
            , {id:24,province:2,city:21,area:212,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:110,y:29.915809}
            , {id:25,province:1,city:11,area:111,type:1,pro:[1,2],name:'东城王府井大街店',info:'王府井apm',x:117.418664,y:30.922343}
            , {id:26,province:1,city:11,area:112,type:5,pro:[2,3,7],name:'西城区',info:'居然之家B馆5楼',x:116.93,y:29.1699}
            , {id:27,province:2,city:22,area:213,type:1,pro:[1,3],name:'福建-福州',info:'体验店1路77号居然之家B馆5楼',x:110.8,y:25.15809}
            , {id:28,province:2,city:22,area:214,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115,y:32.5809}
            , {id:27,province:3,city:31,area:311,type:1,pro:[1,3],name:'福建-福州',info:'体验店1路77号居然之家B馆15楼',x:110.8,y:25.15809}
            , {id:28,province:3,city:31,area:312,type:2,pro:[1,2,3],name:'裕华区裕华路',info:'生活馆1路77号居然之家B馆5楼',x:115,y:32.5809}
        ];
        var $typeData = [
            {
                id:1,name:'单品专区'
            }
            , {
                id:2,name:'多品专区'
            }
            , {
                id:3,name:'AI智感空间'
            }
            , {
                id:4,name:'AI智感体验厅'
            }
            , {
                id:5,name:'AI智感体验馆'
            }
        ];
        var $proData = [
            {
                id:7,name:'空调'
            },
            {
                id:2,name:'洗衣机'
            },
            {
                id:1,name:'冰箱'
            },
            {
                id:5,name:'热水器'
            },
            {
                id:3,name:'厨房电器'
            },
            {
                id:4,name:'水吧'
            },
            {
                id:6,name:'生活家电'
            }

        ]
        var $provinceList = document.getElementById('J-province-list'),
            $cityList = document.getElementById('J-city-list'),
            $areaList = document.getElementById('J-area-list'),
            $typeList = document.getElementById('J-type-list'),
            $proList = document.getElementById('J-pro-list'),
            $scList = document.getElementById('J-sc-list');
        var setList = function(){
            map.clearOverlays();
            var $arr = [];
            console.log(filter);
            for(var i in $data){
                var $row = $data[i];
                var $add = false;
                if(filter.province>0){
                    if($row.province === filter.province) $add = true;
                    else $add = false;
                }
                if(filter.city>0){
                    if($row.city === filter.city) $add = true;
                    else $add = false;
                }
                if($add && filter.area>0){
                    if($row.area === filter.area) $add = true;
                    else $add=false;
                }
                if($add && filter.type>0){
                    if($row.type === filter.type) $add = true;
                    else $add=false;
                }
                if($add && filter.pro>0){
                    var find=false;
                    for(var i in $row.pro){
                        var pro = $row.pro[i];
                        if(pro === filter.pro){
                            find=true;
                            break;
                        }
                    }
                    if(find) $add = true;
                    else $add=false;
                }
                if($add) $arr.push($row);
            }
            //console.log($arr);
            console.log('scList',$scList);
            $(".J-store-ul").removeClass(_ACTIVE_);
            //$scList.innerHTML='<div class="sc-cont"><ul id="list"></ul><div class="store-list-page clear" id="page-nav"><a class="cur" href="javascript:;">1</a><a href="javascript:;">2</a><a href="javascript:;">3</a></div></div>';
            list.innerHTML="";

            setTimeout(function(){
              list.innerHTML="";
                //$("#sc-list").mCustomScrollbar("update");
                var $list = document.getElementById('list');
                var $listCode = [];
                if($arr.length<1){
                    $listCode.push('<div class="line-none">暂无数据!</div>')
                    $($list).html($listCode.join(''));
                    return;
                }
                $arr.forEach(function($row,i){
                    //if(i*1>=10*(filter.page-1)*1 && i*1<filter.page*10){
                    var li = createArea($row,i+1);
                    showPointer($row,i+1);
                    $list.appendChild(li);
                    if(i*1 === 0){
                        window.setTimeout(function(){
                            li.click();
                        },1000);
                    }
                    //}
                });
                //var counter = Math.ceil($arr.length/10);
                counter = $arr.length;
                var $nav = document.getElementById('page-nav');
                $nav.innerHTML = '';
                console.log(counter,$nav);
                $(".J-store-ul").removeClass(_ACTIVE_);
                for(var i=0;i<counter;i++)(function(i){
                    var a = document.createElement('a');
                    if(i === (filter.page-1)){
                        a.className = 'cur';
                    }
                    a.addEventListener('click',function(){
                        filter.page = i+1;
                        setList();
                    });
                    a.innerText = i+1;
                    //$nav.appendChild(a);
                })(i);
                $("#J-sc-list").mCustomScrollbar("update");
                $("#J-province-list").mCustomScrollbar("update");
                $("#J-city-list").mCustomScrollbar("update");
                $("#J-area-list").mCustomScrollbar("update");
                $("#J-pro-list").mCustomScrollbar("update");
                //$(this).find(".store-ul").removeClass(_ACTIVE_);
            },1000);
        };
        function getNumberLabel(number) {
            var offsetSize = new BMap.Size(0, 0);
            var labelStyle = {
                color: "#fff",
                backgroundColor: "0.05",
                border: "0"
            };
            //不同数字长度需要设置不同的样式。
            switch((number + '').length) {
                case 1:
                    labelStyle.fontSize = "14px";
                    offsetSize = new BMap.Size(8, 4);
                    break;
                case 2:
                    labelStyle.fontSize = "12px";
                    offsetSize = new BMap.Size(6, 4);
                    break;
                case 3:
                    labelStyle.fontSize = "10px";
                    offsetSize = new BMap.Size(-2, 4);
                    break;
                default:
                    break;
            }

            var label = new BMap.Label(number, {
                offset: offsetSize
            });
            label.setStyle(labelStyle);
            return label;
        }
        var infoOverlayItems={};
        function InfoOverlay(point, row) {
            this._point = point;
            this._id = 'id-'+row.id;
            this.row = row;
        }
        InfoOverlay.prototype = new BMap.Overlay();
        InfoOverlay.prototype.initialize = function(map) {
            this._map = map;
            var $div = this._div = document.createElement("div");
            $div.setAttribute("id", "row-"+this.row.id);
            $div.style.position = "absolute";
            $div.className = 'info-overlay';
            $div.style.zIndex = BMap.Overlay.getZIndex(this.row.y*-1);
            // $div.style.height = "100px";
            $div.style.width = "320px";
            $div.style.display = 'none';
            $div.style.backgroundColor = "#ffffff";
            $div.innerHTML = "<div class='contText'><span class='s-Name'>"+this.row.name+"</span><span class='s-Add'>"+this.row.info+"</span></div><span class='inforLink'><a class='linkBtn' href='buy-details.html'>查看详情</a></span>";
            var closeBtn = document.createElement('em');
            closeBtn.innerText ='×';
            var that = this;
            closeBtn.addEventListener('click',function(){
                that.close();
            });
            $div.appendChild(closeBtn);
            map.getPanes().labelPane.appendChild($div);
            infoOverlayItems['row-'+this.row.id] = $div;
            return $div;
        }
        InfoOverlay.prototype.close = function(){
            for(var i in infoOverlayItems){
                var item = infoOverlayItems[i];
                item.style.display = 'none';
            }
            //this._div.style.display = 'none';
        },
            InfoOverlay.prototype.show = function(){
                console.log('show');
                this.close();
                this._div.style.display = 'block';
            },
            InfoOverlay.prototype.draw = function() {
                var map = this._map;
                var pixel = map.pointToOverlayPixel(this._point);
                this._div.style.left = pixel.x - 150 + "px";
                this._div.style.top = pixel.y - 200 + "px";
            }
        var showPointer = function(row,i){
            //var infoWindow = new BMap.InfoWindow("<div>"+row.info+"</div><span class='inforLink'><a href=\"buy-details.html\">查看详情</a></span>", opts);  // 创建信息窗口对象

            var point = new BMap.Point(row.x,row.y);
            map.centerAndZoom(point, 18);
            var myIcon = new BMap.Icon("static/img/store-num-bg.png", new BMap.Size(28, 35), {
                anchor: new BMap.Size(0, 35)
            });
            var marker = new BMap.Marker(point,{icon:myIcon});
            marker.addEventListener('click', function(){
                //map.openInfoWindow(row.infoWindow, point);
                row.infoOverlay.show();
            });
            var infoOverlay = new InfoOverlay(point,row);
            map.addOverlay(infoOverlay);
            map.addOverlay(marker);
            marker.setLabel(getNumberLabel(i));
            row.infoOverlay = infoOverlay;
            //marker.setAnimation(BMAP_ANIMATION_BOUNCE);
        };
        var createArea = function(row,num){
            var li = document.createElement('li');
            var i = document.createElement('i');
            var span = document.createElement('span');
            var img = document.createElement('img');
            var a = document.createElement('a');
            var p = document.createElement('p');
            p.className = 'addressTxt';
            a.innerText = row.name;
            if($data.type === 1){
                img.src = 'static/img/store-num-bg.png';
            }else{
                img.src = 'static/img/store-num-bg.png';
            }
            p.innerText = row.info;
            span.innerText = num;
            i.appendChild(span);
            i.appendChild(img);
            li.appendChild(i);
            li.appendChild(a);
            li.appendChild(p);
            li.addEventListener('click',function(){
                var point = new BMap.Point(row.x, row.y);
                map.centerAndZoom(point, 15);
                console.log(row);
                setTimeout(function(){
                    row.infoOverlay.show();
                },100);
                $(".J-store-ul").removeClass(_ACTIVE_);
                //row.infoOverlay.style.display = 'block';
                //map.openInfoWindow(row.infoWindow, map.getCenter());
            });
            return li;
        };
        var createBtn = function(id,text){
            var li = document.createElement('li')
                , span = document.createElement('span');
            span.innerText = text;
            li.appendChild(span);
            return li;
        };
        $provinceList.innerHTML='';
        var filter = {
            province:0,
            city:0,
            area:0,
            type:0,
            pro:0,
            page:1
        };
        var map = new BMap.Map("storeMap");
        map.enableScrollWheelZoom(true);
        var provinceClick=function(province){
            $provinceList.parentNode.parentNode.childNodes[0].innerText = province.name;
            $cityList.innerHTML='';
            filter = {
                province:province.id
                , city:0
                , area:0
                , type:0
                , pro:0
                , page:1
            };

            $cityList.parentNode.parentNode.childNodes[0].innerText = '市区';
            $areaList.parentNode.parentNode.childNodes[0].innerText = '区县';
            $cityList.innerHTML='';
            $areaList.innerHTML='';
            province.city.forEach(function(city,j){
                console.log('province',province,city);
                $(".J-store-ul").removeClass(_ACTIVE_);
                province.city.forEach(function(city,j){
                    console.log('city.id,j,def.c:',city.id,j,$def.c,city.name);
                    if(province.id === $def.p && city.id === $def.c){
                        $cityList.parentNode.parentNode.childNodes[0].innerText = city.name;
                        city.area.forEach(function(area,j){
                            console.log('area.id,j,def.c:',area.id,j,$def.a,area.name);
                            if(area.id === $def.a){
                                $areaList.parentNode.parentNode.childNodes[0].innerText = area.name;
                                var areaSelect = document.getElementById('J-area-select');
                                areaSelect.style.display = "block";
                                $areaList.innerHTML = '';
                                city.area.forEach(function(area,k){
                                    var areaBtn = createBtn(area.id,area.name);

                                    $areaList.appendChild(areaBtn);
                                    areaBtn.addEventListener('click',function(){
                                        $areaList.parentNode.parentNode.childNodes[0].innerText = area.name;
                                        filter = {
                                            province:province.id
                                            , city:city.id
                                            , area:area.id
                                            , type:0
                                            , pro:0
                                            , page:1
                                        };
                                        setList();
                                    });
                                });
                                setList();
                            }
                        });
                    }
                });
                var cityBtn = createBtn(city.id,city.name);
                $cityList.appendChild(cityBtn);
                cityBtn.addEventListener('click',function(){
                    var areaSelect = document.getElementById('J-area-select');
                    areaSelect.style.display = "block";
                    $cityList.parentNode.parentNode.childNodes[0].innerText = city.name;
                    filter = {
                        province:province.id
                        , city:city.id
                        , area:0
                        , type:0
                        , pro:0
                        , page:1
                    };
                    setList();
                    $areaList.innerHTML = '';
                    $areaList.parentNode.parentNode.childNodes[0].innerText = '区县';
                    city.area.forEach(function(area,j){
                        var areaBtn = createBtn(area.id,area.name);

                        $areaList.appendChild(areaBtn);

                        areaBtn.addEventListener('click',function(){
                            $areaList.parentNode.parentNode.childNodes[0].innerText = area.name;
                            filter = {
                                province:province.id
                                , city:city.id
                                , area:area.id
                                , type:0
                                , pro:0
                                , page:1
                            };
                            setList();
                        });
                    });
                });
            });
            setList();
        };
        $provinceData.forEach(function(province,i){
            var provinceBbtn = createBtn(province.id,province.name);
            provinceBbtn.addEventListener('click',function(){
                provinceClick(province);
            });
            $provinceList.appendChild(provinceBbtn);
            if(province.id === $def.p){
                $provinceList.parentNode.parentNode.childNodes[0].innerText = province.name;
                //$cityList.parentNode.parentNode.childNodes[0].innerText = '市区';


                filter = {
                    province:province.id
                    , city: $def.c
                    , kid:0
                    , type:0
                    , pro:0
                    , page:1
                };
               provinceClick(province,true);
            }
        });

        $typeData.forEach(function(type,i){
            var btn = createBtn(type.id,type.name);
            btn.addEventListener('click',function(){
                $typeList.parentNode.parentNode.childNodes[0].innerText = type.name;
                filter.type = type.id;
                filter.pro = 0;
                setList();
            });
            if(i*1 === 0){
                $typeList.parentNode.parentNode.childNodes[0].innerText = '门店类别';
            }
            $typeList.appendChild(btn);
        });
        $proList.innerHTML='';
        $proData.forEach(function(pro,i){
            var btn = createBtn(pro.id,pro.name);
            $proList.appendChild(btn);
            btn.addEventListener('click',function(){
                $proList.parentNode.parentNode.childNodes[0].innerText = pro.name;
                filter.pro = pro.id;
                setList();
            });
            if(i*1 === 0){
                $proList.parentNode.parentNode.childNodes[0].innerText = '产品类别';
            }
        });
    }

})