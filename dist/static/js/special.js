$(document).ready(function(){

    //专题页视频弹框
    function specialVideo(){
        var $url;
        $url= $(".J-special-play-1").attr("data-url");
        $(document).on("click",".J-special-play-1,.J-kvPlay,.J-livePlay,.J-videoPlay,.J-GustPlay",function(){
            $(".J-special-video-popup").stop().fadeIn();
            $url=$(this).attr("data-url");
            playVideo($url);
        });

        $(".J-special-video-popup .J-closeVideo").click(function(){
            $(".J-special-video-popup").stop().fadeOut();
            $(".J-special-video-popup #video").empty();
        });

        function playVideo(url){
            jwplayer('video').setup({
                flashplayer: "../js/video/jwplayer.flash.swf",//调用播放器
                file: url,
                play:true,
                // image: src,
                width:'100%',
                height:'100%',
                events:{
                    onReady:function(){
                        this.play();
                    }
                }
            });
        }
        if($(".J-special-video-popup").length > 0){
            playVideo();
        }
    }
    specialVideo();

    //精彩瞬间轮播图
    // function wonderful(){
    //     var _slideLen = $(".special-wonderful-thumbs .swiper-slide").length;
    //     console.log(_slideLen);
    //     if($(window).width() > 768){
    //         if(_slideLen > 4){
    //             var galleryTop = new Swiper('.special-wonderful-top .gallery-top', {
    //                 nextButton: '.special-wonderful-top .swiper-button-next',
    //                 prevButton: '.special-wonderful-top .swiper-button-prev',
    //                 pagination: '.special-wonderful-top .swiper-pagination',
    //                 paginationType: 'fraction',
    //                 spaceBetween: 10,
    //                 loop:true,
    //                 loopedSlides: 9,
    //             });
    //             var galleryThumbs = new Swiper('.special-wonderful-thumbs .gallery-thumbs', {
    //                 spaceBetween: 10,
    //                 slidesPerView: 5,
    //                 centeredSlides : true,
    //                 touchRatio: 0.2,
    //                 loop:true,
    //                 loopedSlides: 9,
    //                 slideToClickedSlide: true,
    //                 onTap: function() {
    //                     galleryTop.slideTo(galleryThumbs.clickedIndex)
    //                 }
    //             });
    //         }else{
    //             var galleryTop = new Swiper('.special-wonderful-top .gallery-top', {
    //                 nextButton: '.special-wonderful-top .swiper-button-next',
    //                 prevButton: '.special-wonderful-top .swiper-button-prev',
    //                 pagination: '.special-wonderful-top .swiper-pagination',
    //                 paginationType: 'fraction',
    //                 spaceBetween: 10,
    //                 loop:false,
    //                 loopedSlides: 9,
    //             });
    //             var galleryThumbs = new Swiper('.special-wonderful-thumbs .gallery-thumbs', {
    //                 spaceBetween: 10,
    //                 slidesPerView: 5,
    //                 centeredSlides : true,
    //                 touchRatio: 0.2,
    //                 loop:false,
    //                 loopedSlides: 9,
    //                 slideToClickedSlide: true,
    //                 onTap: function() {
    //                     galleryTop.slideTo(galleryThumbs.clickedIndex)
    //                 }
    //             });
    //         }
    //     } else{
    //         if(_slideLen > 4){
    //             var galleryTop = new Swiper('.special-wonderful-top .gallery-top', {
    //                 nextButton: '.special-wonderful-top .swiper-button-next',
    //                 prevButton: '.special-wonderful-top .swiper-button-prev',
    //                 pagination: '.special-wonderful-top .swiper-pagination',
    //                 paginationType: 'fraction',
    //                 spaceBetween: 10,
    //                 loop:true,
    //                 loopedSlides: 5,
    //             });
    //             var galleryThumbs = new Swiper('.special-wonderful-thumbs .gallery-thumbs', {
    //                 spaceBetween: 10,
    //                 slidesPerView: 3,
    //                 centeredSlides : true,
    //                 touchRatio: 0.2,
    //                 loop:true,
    //                 loopedSlides: 5,
    //                 slideToClickedSlide: true,
    //                 onTap: function() {
    //                     galleryTop.slideTo(galleryThumbs.clickedIndex)
    //                 }
    //             });
    //         }else{
    //             var galleryTop = new Swiper('.special-wonderful-top .gallery-top', {
    //                 nextButton: '.special-wonderful-top .swiper-button-next',
    //                 prevButton: '.special-wonderful-top .swiper-button-prev',
    //                 pagination: '.special-wonderful-top .swiper-pagination',
    //                 paginationType: 'fraction',
    //                 spaceBetween: 10,
    //                 loop:false,
    //                 loopedSlides: 5,
    //             });
    //             var galleryThumbs = new Swiper('.special-wonderful-thumbs .gallery-thumbs', {
    //                 spaceBetween: 10,
    //                 slidesPerView: 3,
    //                 centeredSlides : true,
    //                 touchRatio: 0.2,
    //                 loop:false,
    //                 loopedSlides: 5,
    //                 slideToClickedSlide: true,
    //                 onTap: function() {
    //                     galleryTop.slideTo(galleryThumbs.clickedIndex)
    //                 }
    //             });
    //         }
    //     };
    //
    //     galleryTop.params.control = galleryThumbs;
    //     galleryThumbs.params.control = galleryTop;
    // }
    function wonderful(){
        var viewSwiper = new Swiper('.special-wonderful-top .gallery-top', {
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            pagination: '.special-wonderful-top .swiper-pagination',
            paginationType: 'fraction',
            onSlideChangeStart: function() {
                updateNavPosition();
            }
        })

        $('.special-wonderful-top .swiper-button-prev,.special-wonderful-thumbs .swiper-button-prev').on('click', function(e) {
            e.preventDefault();
            if (viewSwiper.activeIndex == 0) {
                viewSwiper.slideTo(viewSwiper.slides.length - 1, 300);
                return;
            }
            viewSwiper.slidePrev();
        })
        $('.special-wonderful-top .swiper-button-next,.special-wonderful-thumbs .swiper-button-next').on('click', function(e) {
            e.preventDefault();
            if (viewSwiper.activeIndex == viewSwiper.slides.length - 1) {
                viewSwiper.slideTo(0, 300);
                return;
            }
            viewSwiper.slideNext();
        })

        var previewSwiper = new Swiper('.special-wonderful-thumbs .gallery-thumbs', {
            //visibilityFullFit: true,
            slidesPerView: 5,
            spaceBetween: 10,
            allowTouchMove: false,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            onTap: function() {
                viewSwiper.slideTo(previewSwiper.clickedIndex);
            }
        })

        function updateNavPosition() {
            $('.special-wonderful-thumbs .active-nav').removeClass('active-nav');
            var activeNav = $('.special-wonderful-thumbs .swiper-slide').eq(viewSwiper.activeIndex).addClass('active-nav');
            if (!activeNav.hasClass('swiper-slide-visible')) {
                if (activeNav.index() > previewSwiper.activeIndex) {
                    var thumbsPerNav = Math.floor(previewSwiper.width / activeNav.width()) - 1;
                    previewSwiper.slideTo(activeNav.index() - thumbsPerNav);
                } else {
                    previewSwiper.slideTo(activeNav.index());
                }
            }
        }
    }
    if($(".gallery-top").length > 0){
        wonderful();
    }

    //品牌发布-AWE
    function AWEBanner(){
        var mySwiper1 = new Swiper ('.J-special-banner .swiper-container', {
            nextButton: '.J-special-banner .swiper-button-next',
            prevButton: '.J-special-banner .swiper-button-prev',
            pagination: '.J-special-banner .swiper-pagination',
            paginationClickable :true,
            calculateHeight : true,
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : false,
            paginationBulletRender: function (swiper, index, className) {
                return '<span class="' + className + '">0' + (index + 1) + '<em><i></i></em></span>';
            },
            onInit:function(swiper){
                if($(window).width() > 768){
                    $(".J-special-banner .swiper-slide").each(function(){
                        var nextImg = $(".J-special-banner .swiper-slide-next").find("img").attr("src");
                        var prevImg = $(".J-special-banner .swiper-slide-prev").find("img").attr("src");
                        $(".J-special-banner .swiper-button-next").find("img").attr("src",nextImg);
                        $(".J-special-banner .swiper-button-prev").find("img").attr("src",prevImg);
                    })
                }
            },
            onSlideChangeStart: function (swiper) {
                if($(window).width() > 768){
                    $(".J-special-banner .swiper-slide").each(function(){
                        var nextImg = $(".J-special-banner .swiper-slide-next").find("img").attr("src");
                        var prevImg = $(".J-special-banner .swiper-slide-prev").find("img").attr("src");
                        $(".J-special-banner .swiper-button-next").find("img").attr("src",nextImg);
                        $(".J-special-banner .swiper-button-prev").find("img").attr("src",prevImg);
                    })
                }
            },
            onSwiperCreated: function(mySwiper){
                if($.browser.msie&&($.browser.version === "9.0")){
                    var $_pageLen = $(".J-special-banner .swiper-pagination span");
                    $_pageLen.each(function(){
                        var $_index = $(this).index() + 1;
                        $(this).html('0'+$_index);
                    })
                }
            }
        })
    }
    AWEBanner();

    //全新blanc套系轮播
    function nestedSwiper(){
        var mySwiper2 = new Swiper ('.J-AWE-nested-swiper .swiper-container', {
            nextButton: '.J-AWE-nested-swiper .swiper-button-next',
            prevButton: '.J-AWE-nested-swiper .swiper-button-prev',
            pagination: '.J-AWE-nested-swiper .swiper-pagination',
            paginationType: 'fraction',
            paginationClickable :true,
            calculateHeight : true,
            centeredSlides : true,
            slidesPerView :'auto',
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : false,
        })
    }
    nestedSwiper();

    function eplanSwiper(){
        var mySwiper3 = new Swiper ('.J-special-eplan-swiper .swiper-container', {
            nextButton: '.J-special-eplan-swiper .swiper-button-next',
            prevButton: '.J-special-eplan-swiper .swiper-button-prev',
            pagination: '.J-special-eplan-swiper .swiper-pagination',
            paginationClickable :true,
            calculateHeight : true,
            centeredSlides : true,
            slidesPerView :'auto',
            autoplay:5000,
            autoplayDisableOnInteraction : false,
            loop : true,
        })
    }
    eplanSwiper();

    function momentSwiper(){
        if($(window).width() > 768){
            var galleryLeft = new Swiper('.special-moment-l .swiper-container', {
                pagination: '.special-moment-l .swiper-pagination',
                paginationType: 'fraction',
                spaceBetween: 10,
                autoplay:5000,
                loop:true,
                loopedSlides: 6,
                autoplayDisableOnInteraction : false,
            });
            var galleryRight = new Swiper('.special-moment-r .swiper-container', {
                nextButton: '.special-moment-r .swiper-button-next',
                prevButton: '.special-moment-r .swiper-button-prev',
                spaceBetween: 10,
                slidesPerView: 3,
                touchRatio: 0.2,
                direction: 'vertical',
                centeredSlides : true,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true,
                autoplayDisableOnInteraction : false,
            });
        } else{
            var galleryLeft = new Swiper('.special-moment-l .swiper-container', {
                pagination: '.special-moment-l .swiper-pagination',
                paginationType: 'fraction',
                spaceBetween: 10,
                autoplay:5000,
                loop:true,
                loopedSlides: 6,
                autoplayDisableOnInteraction : false,
            });
            var galleryRight = new Swiper('.special-moment-r .swiper-container', {
                nextButton: '.special-moment-r .swiper-button-next',
                prevButton: '.special-moment-r .swiper-button-prev',
                spaceBetween: 10,
                slidesPerView: 3,
                touchRatio: 0.2,
                centeredSlides : true,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true,
                autoplayDisableOnInteraction : false,
            });
        }
        galleryLeft.params.control = galleryRight;
        galleryRight.params.control = galleryLeft;
    }
    if($(".special-moment-l").length > 0){
        momentSwiper();
    }


    function salonBannerSwiper(){
        var _slideLen = $(".J-special-salon-banner .swiper-slide").length;
        if(_slideLen > 1){
            var mySwiper = new Swiper ('.J-special-salon-banner .swiper-container', {
                nextButton: '.J-special-salon-banner .swiper-button-next',
                prevButton: '.J-special-salon-banner .swiper-button-prev',
                pagination: '.J-special-salon-banner .swiper-pagination',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : true,
            })
        }else{
            $(".J-special-salon-banner .swiper-slide").addClass("stop-swiping");
            $(".J-special-salon-banner .swiper-button-next").hide();
            $(".J-special-salon-banner .swiper-button-prev").hide();
            $(".J-special-salon-banner .swiper-pagination").hide();
            var mySwiper = new Swiper ('.J-special-salon-banner .swiper-container', {
                noSwiping : true,
                noSwipingClass : 'stop-swiping',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplayDisableOnInteraction : false,
            })
        }

    }
    salonBannerSwiper();

    function salonGuest(){
        if($(window).width() > 768){
            var mySwiper2 = new Swiper ('.J-salon-guest-swiper .swiper-container', {
                nextButton: '.J-salon-guest-swiper .swiper-button-next',
                prevButton: '.J-salon-guest-swiper .swiper-button-prev',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplay:5000,
                effect: 'coverflow',
                autoplayDisableOnInteraction : false,
                loop : true,
                slidesPerView: 'auto',
                coverflow: {
                    rotate: 0,
                    stretch: 0,
                    depth: 200,
                    modifier: 1,
                    slideShadows : true
                }
            })
        } else{
            var mySwiper2 = new Swiper ('.J-salon-guest-swiper .swiper-container', {
                nextButton: '.J-salon-guest-swiper .swiper-button-next',
                prevButton: '.J-salon-guest-swiper .swiper-button-prev',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                loop : true,
            })
        }
    }
    salonGuest();

    function salonSwiper(){
        if($(window).width() > 768){
            var galleryTop = new Swiper('.special-salon-moment .gallery-top', {
                nextButton: '.special-salon-moment .swiper-button-next',
                prevButton: '.special-salon-moment .swiper-button-prev',
                pagination: '.special-salon-moment .swiper-pagination',
                paginationType: 'fraction',
                spaceBetween: 10,
                loop:true,
                loopedSlides: 6,
            });
            var galleryThumbs = new Swiper('.special-salon-moment .gallery-thumbs', {
                spaceBetween: 10,
                slidesPerView: 5,
                centeredSlides : true,
                touchRatio: 0.2,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true
            });
        } else{
            var galleryTop = new Swiper('.special-salon-moment .gallery-top', {
                nextButton: '.special-salon-moment .swiper-button-next',
                prevButton: '.special-salon-moment .swiper-button-prev',
                pagination: '.special-salon-moment .swiper-pagination',
                paginationType: 'fraction',
                spaceBetween: 10,
                loop:true,
                loopedSlides: 6,
            });
            var galleryThumbs = new Swiper('.special-salon-moment .gallery-thumbs', {
                spaceBetween: 10,
                slidesPerView: 3,
                centeredSlides : true,
                touchRatio: 0.2,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true
            });
        }
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
    }
    if($(".special-salon-moment").length > 0){
        // salonSwiper();
    }


    function waicSwiper(){
        if($(window).width() > 768){
            var galleryTop = new Swiper('.waic-wonderful-top .gallery-top', {
                paginationType: 'fraction',
                spaceBetween: 10,
                loop:true,
                loopedSlides: 6,
            });
            var galleryThumbs = new Swiper('.waic-wonderful-thumbs .gallery-thumbs', {
                nextButton: '.waic-wonderful-thumbs .swiper-button-next',
                prevButton: '.waic-wonderful-thumbs .swiper-button-prev',
                spaceBetween: 20,
                slidesPerView: 4,
                // centeredSlides : true,
                touchRatio: 0.2,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true
            });
        } else{
            var galleryTop = new Swiper('.waic-wonderful-top .gallery-top', {
                paginationType: 'fraction',
                spaceBetween: 10,
                loop:true,
                loopedSlides: 6,
            });
            var galleryThumbs = new Swiper('.waic-wonderful-thumbs .gallery-thumbs', {
                nextButton: '.waic-wonderful-thumbs .swiper-button-next',
                prevButton: '.waic-wonderful-thumbs .swiper-button-prev',
                spaceBetween: 20,
                slidesPerView: 3,
                // centeredSlides : true,
                touchRatio: 0.2,
                loop:true,
                loopedSlides: 6,
                slideToClickedSlide: true
            });
        }
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
    }
    if($(".waic-wonderful-top").length > 0){
        waicSwiper();
    }

    function idealistsSwiper(){
        if($(window).width() > 1024){
            var swiperJB = new Swiper('.idealists-guest-swiper .swiper-container', {
                nextButton: '.idealists-guest-swiper .swiper-button-next',
                prevButton: '.idealists-guest-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 30,
                slidesPerView: 4,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperGuest = new Swiper('.i-guest-video .swiper-container', {
                nextButton: '.i-guest-video .swiper-button-next',
                prevButton: '.i-guest-video .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 30,
                slidesPerView: 2,
                // loop:false,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperGuest2 = new Swiper('.t-guest-video .swiper-container', {
                nextButton: '.t-guest-video .swiper-button-next',
                prevButton: '.t-guest-video .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 30,
                slidesPerView: 2,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperpro = new Swiper('.idealists-pro-swiper .swiper-container', {
                nextButton: '.idealists-pro-swiper .swiper-button-next',
                prevButton: '.idealists-pro-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 20,
                slidesPerView: 3,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var mySwiperJC = new Swiper ('.t-Wf-swiper .swiper-container', {
                nextButton: '.t-Wf-swiper .swiper-button-next',
                prevButton: '.t-Wf-swiper .swiper-button-prev',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplay:5000,
                effect: 'coverflow',
                autoplayDisableOnInteraction : false,
                // loop : true,
                slidesPerView: 'auto',
                coverflow: {
                    rotate: 0,
                    stretch: 0,
                    depth: 200,
                    modifier: 1,
                    slideShadows : true
                }
            })
        }else if($(window).width() > 768){
            var swiperJB = new Swiper('.idealists-guest-swiper .swiper-container', {
                nextButton: '.idealists-guest-swiper .swiper-button-next',
                prevButton: '.idealists-guest-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                slidesPerView: 2,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperpro = new Swiper('.idealists-pro-swiper .swiper-container', {
                nextButton: '.idealists-pro-swiper .swiper-button-next',
                prevButton: '.idealists-pro-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                slidesPerView: 2,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
        }else{
            var swiperJB = new Swiper('.idealists-guest-swiper .swiper-container', {
                nextButton: '.idealists-guest-swiper .swiper-button-next',
                prevButton: '.idealists-guest-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                slidesPerView: 1,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperpro = new Swiper('.idealists-pro-swiper .swiper-container', {
                nextButton: '.idealists-pro-swiper .swiper-button-next',
                prevButton: '.idealists-pro-swiper .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                slidesPerView: 1,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var swiperGuest = new Swiper('.i-guest-video .swiper-container', {
                nextButton: '.i-guest-video .swiper-button-next',
                prevButton: '.i-guest-video .swiper-button-prev',
                calculateHeight : true,
                autoHeight: true,
                spaceBetween: 10,
                slidesPerView: 1,
                // loop:false,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
            var mySwiperJC = new Swiper ('.t-Wf-swiper .swiper-container', {
                nextButton: '.t-Wf-swiper .swiper-button-next',
                prevButton: '.t-Wf-swiper .swiper-button-prev',
                paginationClickable :true,
                calculateHeight : true,
                centeredSlides : true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
                // loop : true,
            });
            var swiperGuest2 = new Swiper('.t-guest-video .swiper-container', {
                nextButton: '.t-guest-video .swiper-button-next',
                prevButton: '.t-guest-video .swiper-button-prev',
                calculateHeight : true,
                autoHeight: false,
                spaceBetween: 10,
                slidesPerView: 1,
                // loop:true,
                autoplay:5000,
                autoplayDisableOnInteraction : false,
            });
        }

    }
    if($(".idealists-modular,.thirteen-modular").length > 0){
        idealistsSwiper();
    }

    function JYvideoSlide(){
        var _slideLen = $(".essence-guest-video .swiper-slide").length;
        if(_slideLen > 4){

        }
        var swiper = new Swiper('.essence-guest-video .swiper-container', {
            nextButton: '.essence-guest-video .swiper-button-next',
            prevButton: '.essence-guest-video .swiper-button-prev',
            calculateHeight : true,
            autoHeight: true,
            spaceBetween: 10,
            slidesPerView: 4,
            loop:false,
            // autoplay:5000,
            autoplayDisableOnInteraction : false,
        });
    }
    if($(".essence-guest-video").length > 0){
        JYvideoSlide();
    }



});

//当页面尺寸改变时执行
$(window).resize(function(){




});