var nowPage=1;
$(document).ready(function () {

    $("#J-addNew").click(function(){
        $(".J-address-list").hide();
        $(".J-add-address").css({'height':'auto','opacity':'1','overflow':'inherit'})
    })
    $(".J-list").on("click",".J-edit",function(){
    	var $A_name = $(this).parents(".J-take-address").find(".a-name").text(),
            $A_tel = $(this).parents(".J-take-address").find(".a-tel").text(),
			$A_province = $(this).parents(".J-take-address").find(".s-province").text(),
            $A_city = $(this).parents(".J-take-address").find(".s-city").text(),
            $A_area = $(this).parents(".J-take-address").find(".s-area").text(),
            $A_details = $(this).parents(".J-take-address").find(".s-details").text();
    	$(".J-add-form").find(".J-emptyName").attr('value',$A_name);
        $(".J-add-form").find(".J-tel").attr('value',$A_tel);
        $(".J-add-form .province").find(".selector-name").text($A_province);
        $(".J-add-form .city").find(".selector-name").text($A_city);
        $(".J-add-form .district").find(".selector-name").text($A_area);
        $(".J-add-form .add-input-con").find(".details-add").attr('value',$A_details);
        $(".J-address-list").hide();
        $(".J-add-address").css({'height':'auto','opacity':'1','overflow':'inherit'})
    })
    $(".J-addressReturn a").click(function(){
        $(".J-address-list").show();
        $(".J-add-address").css({'height':'0','opacity':'0','overflow':'hidden'})
    })
    $(document).on("click",".J-save-qd",function(){
        $(".J-address-list").show();
        $(".J-add-address").css({'height':'0','opacity':'0','overflow':'hidden'})
    })
    $(".J-list").on("click",".J-del",function(){
        $(".J-funPopup").fadeIn(100);
        $(this).parents(".J-take-address").addClass("remove");
        $(this).parents(".J-take-address").siblings().removeClass("remove");
        noData();
    })
	$(".J-confirmDel").click(function(){
        $(".J-funPopup").fadeOut(100);
        $(".J-list").find(".remove").remove();
				setTimeout(function(){
					load(nowPage);
			},500);
	})
	$(".J-closeFunPopup").click(function(){
        $(".J-funPopup").fadeOut(100);
	})



	/*无收货地址时显示提示*/
    function noData(){
        $(".J-list").each(function(){
            var $childLen = $(this).find(".J-take-address").length;
            if($childLen > 0){
                $(".J-noData-con").hide();
                $(".J-page-number").show();
            }else{
                $(".J-noData-con").show();
                $(".J-page-number").hide();
            }
        })
    }

	load(1);
});
var $first=true;
function scrollTo(element,speed) {
	if(!speed)speed = 300;
	if(!element) $("html,body").animate({scrollTop:0},speed);
	else{
		if(element.length>0){
			$("html,body").animate({scrollTop:$(element).offset().top-140},speed);
		}
	}
}
function load(page) {
	var $url = getUrl(page);
	$('#loading').show();
	if(!$first)scrollTo('#loading',500);
	$first=false;
	$('#list').each(function () {
		var $list = this;
		console.log(this);
		$($list).html('');
		var $listCode=[];
		setTimeout(function () {
			$.getJSON($url, function (data) {
				$('#loading').hide();
				if(data.now<1){
					$('#list-none').show();
				}else{
					$($list).show();
				}
				for (var i = 0; i < data.rows.length; i++) {
					var $row = data.rows[i];
					$listCode.push(
						'<div class="take-address J-take-address '+($row.default||'')+' clear">'+
						'<ul>'+
						'<li><span class="a-type">姓名：</span><span class="a-name">'+$row.name+'</span></li>'+
						'<li><span class="a-type">电话：</span><span class="a-tel">'+$row.tel+'</span></li>'+
						'<li><span class="a-type">地址：</span><span class="a-add">' +
						'<em class="s-province">'+$row.add_province+'</em>' +
                        '<em class="s-city">'+$row.add_city+'</em>' +
                        '<em class="s-area">'+$row.add_area+'</em>' +
                        '<em class="s-details">'+$row.add_details+'</em>' +
						'</span></li>'+
						'</ul>'+
						'<div class="J-editBtn editBtn"><a class="J-edit edit" href="'+$row.edit+'">编辑</a><a class="J-del del" href="'+$row.del+'">删除</a></div>'+
						'</div>');
				}
				$($list).html($listCode.join(''));
				var $nav = [];
				var $navUl = $('#list-page');
				$nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
				var $num=5;
				if(data.total>$num) {

					if(data.total<5){
						$num = data.total;
					}
					if (data.now < $num) {
						for (var i = 1; i <= $num; i++) {
							var $str = '0' + i;
							var $now = '';
							if (i === data.now) $now = 'class="active"';
							$nav.push('<li ' + $now + '><a href="javascript:load(' + i + ');">' + $str + '</a></li>');

						}
						if(data.total>$num) $nav.push('<li>...</li>');
					} else if (data.now > data.total - $num) {
						$nav.push('<li>...</li>');
						for (var i = data.total - $num; i <= data.total; i++) {
							var $str = i + '';
							if ($str.length <= 1) $str = '0' + $str;
							var $now = '';
							if (i === data.now) $now = 'class="active"';
							$nav.push('<li ' + $now + '><a href="javascript:load(' + i + ');">' + $str + '</a></li>');
						}
					} else {
						$nav.push('<li>...</li>');
						for (var i = data.now - 1; i < data.now + 4; i++) {
							var $str = i + '';
							if ($str.length <= 1) $str = '0' + $str;
							var $now = '';
							if (i === data.now) $now = 'class="active"';
							$nav.push('<li ' + $now + '><a href="javascript:load(' + i + ');">' + $str + '</a></li>');
						}
						$nav.push('<li>...</li>');
					}
					$($navUl).html($nav.join(''));
					if(data.now+4 < data.total) $navUl.append('<li><a href="javascript:load('+(data.total)+');">'+(data.total)+'</a></li>');
          $navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
					nowPage = data.now;
				}
			});
		},2000);
	});
}
function getUrl(page){
	page='';
    return "static/json/data-list-huiyuan-take.json";
}