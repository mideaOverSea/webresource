function test(){
    if($('.num-small').val()*1>=$('.num-big').val()*1){
        alert('请输入正确的积分区间');
        return true;
    }else{
        return false;
    }
}
$(document).ready(function () {

    /*点击兑换判断是否登录*/
    $(".m-club-dh-list").on("click",".J-redeemNow",function(){
        var $href = $(this).attr("data-href");
        if($(".userToggle").is(":hidden")){
            $(this).attr("href","login.html");
        }else{
            $(this).attr("href",$href);
        }
    });

    load(1);
    var $params={
        '000':'001',
        '010':'011',
        '020':'021'
    };
    $('.select-check').each(function () {
        var $ddRows = $(this).find('dd');
        console.log($ddRows);
        $($ddRows).on('click',function () {
            //console.log(this);
            var clickBtn=this;
            $($ddRows).each(function () {
                $(this).removeClass('active');
            })
            $params[clickBtn.dataset.name]=clickBtn.dataset.val;
            $(clickBtn).addClass('active');
        });
    });
    $('.exBtn-submit').on('click',function () {
        if(test()){

        }else{
            $params['small']=$('.num-small').val();
            $params['big']=$('.num-big').val();
            load(1,$params);
        }

    });
    $('.exBtn-cancel').on('click',function () {
        $params={
            '000':'001',
            '010':'011',
            '020':'021'
        };
        $(".ex-intervals input").val("");
        $('.select-check').each(function () {
            var $ddRows = $(this).find('dd');
            console.log($ddRows);
            var i=0;
            $($ddRows).each(function () {
                if(i===0){
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
                i++;
            });
        });
    });
});
var $first=true;
function scrollTo(element,speed) {
    if(!speed)speed = 100;
    if(!element) $("html,body").animate({scrollTop:0},speed);
    else{
        if(element.length>0){
            $("html,body").animate({scrollTop:$(element).offset().top-140},speed);
        }
    }
}
function load(page,params) {
    var $url = getUrl(page,params);
    $('#loading').show();
    if(!$first)scrollTo('#toper',500);
    $first=false;
    $('#J-list').each(function () {
        var $list = this;
        setTimeout(function () {
            $($list).html('');
        },260);
        var $listCode=[];
        setTimeout(function () {
            $.getJSON($url, function (data) {
                $('#loading').hide();
                if(data.now<1){
                    $('#list-none').show();
                }else{
                    $($list).show();
                }
                for (var i = 0; i < data.rows.length; i++) {
                    var row = data.rows[i];
                    $listCode.push('<div class="club-dh-item">'+
                        '<div class="dh-img">' +
                        '<img src="'+row.src+'">' +
                        '<div class="dh-QR"><p><img src="'+row.qrImg+'" alt=""><span>扫一扫，查看礼品详情</span></p></div>'+
                        '</div>'+
                        '<div class="dh-txt">'+
                        '<div class="dh-pro-tit">'+row.tit+'</div><span class="integral">'+row.integral+'</span><a class="J-redeemNow" href="javascript:;" data-href="'+row.href+'">立即兑换</a><a class="J-qr" href="'+row.href+'" data-href="'+row.link+'">查看详情</a>'+
                        '</div>'+
                        '</div>');
                }
                $($list).html($listCode.join(''));
                var nav = [];
                var navUl = $('#list-page');
                //$(c).append(navContainer);


                //$(navContainer).append(navUl);
                nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
                var num=5;
                if(data.total>num) {

                    if(data.total<5){
                        num = data.total;
                    }
                    if (data.now < num) {
                        for (var i = 1; i <= num; i++) {
                            var str = '0' + i;
                            var now = '';
                            if (i === data.now) now = 'class="active"';
                            nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');

                        }
                        if(data.total>num) nav.push('<li>...</li>');
                    } else if (data.now > data.total - num) {
                        nav.push('<li>...</li>');
                        for (var i = data.total - num; i <= data.total; i++) {
                            var str = i + '';
                            if (str.length <= 1) str = '0' + str;
                            var now = '';
                            if (i === data.now) now = 'class="active"';
                            nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
                        }
                    } else {
                        nav.push('<li>...</li>');
                        for (var i = data.now - 1; i < data.now + 4; i++) {
                            var str = i + '';
                            if (str.length <= 1) str = '0' + str;
                            var now = '';
                            if (i === data.now) now = 'class="active"';
                            nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
                        }
                        nav.push('<li>...</li>');
                    }
                    $(navUl).html(nav.join(''));
                    if(data.now+4 < data.total) navUl.append('<li><a href="javascript:load('+(data.total)+');">'+(data.total)+'</a></li>');
                    navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
                }
            });
        },2000);
    });
}
function getUrl(page,params){
    page='';
    console.log(JSON.stringify(params));
    return "static/json/data-list-huiyuan-exchanges.json"+'?params='+encodeURI(JSON.stringify(params));
}

//积分区间验证
function v(){
    if($(".num-small")[0].value*1 >= $(".num-big")[0].value*1){
        console.log('请输入正确的积分区间');
        $(".num-small")[0].value=0;
        $(".num-big")[0].value=0;
    }else{
        $(".num-small")[0].value*=1;
        $(".num-big")[0].value*=1;
    }
}