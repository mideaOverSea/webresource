$(document).ready(function () {

	load(1);
});
var $first=true;
function scrollTo(element,speed) {
	if(!speed)speed = 300;
	if(!element) $("html,body").animate({scrollTop:0},speed);
	else{
		if(element.length>0){
			$("html,body").animate({scrollTop:$(element).offset().top-140},speed);
		}
	}
}
function load(page) {
	var $url = getUrl(page);
	$('#loading').show();
	if(!$first)scrollTo('#loading',500);
	$first=false;
	$('#J-list').each(function () {
		var $list = this;
		var $tbody= $($list).find("tbody");
		$tbody.html('');
		$($list).hide();
		var $listCode=[];
		setTimeout(function () {
			$.getJSON($url, function (data) {
				$('#loading').hide();
				if(data.now<1){
                    $(".J-noData-con").show();
                    $(".J-page-number").hide();
				}else{
                    $(".J-noData-con").hide();
                    $(".J-page-number").show();
					$($list).show();
				}
				for (var i = 0; i < data.rows.length; i++) {
					var row = data.rows[i];
					$listCode.push('<tr>'+
						'<td>'+row.a1+'</td>'+
						'<td>'+row.a2+'</td>'+
						'<td>'+row.a3+'</td>'+
						'<td>'+row.a4+'</td>'+
						'<td class="plus">'+row.a5+'</td>'+
						'</tr>');
				}
				$tbody.html($listCode.join(''));
				var nav = [];
				var navUl = $('#list-page');
				//$(c).append(navContainer);
				//$(navContainer).append(navUl);
				nav.push('<li><a href="javascript:load('+(data.now-1)+');">上一页</a></li>');
				var num=5;
				if(data.total>num) {

					if(data.total<5){
						num = data.total;
					}
					if (data.now < num) {
						for (var i = 1; i <= num; i++) {
							var str = '0' + i;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');

						}
						if(data.total>num) nav.push('<li>...</li>');
					} else if (data.now > data.total - num) {
						nav.push('<li>...</li>');
						for (var i = data.total - num; i <= data.total; i++) {
							var str = i + '';
							if (str.length <= 1) str = '0' + str;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
						}
					} else {
						nav.push('<li>...</li>');
						for (var i = data.now - 1; i < data.now + 4; i++) {
							var str = i + '';
							if (str.length <= 1) str = '0' + str;
							var now = '';
							if (i === data.now) now = 'class="active"';
							nav.push('<li ' + now + '><a href="javascript:load(' + i + ');">' + str + '</a></li>');
						}
						nav.push('<li>...</li>');
					}
					$(navUl).html(nav.join(''));
					if(data.now+4 < data.total) navUl.append('<li><a href="javascript:load('+(data.total)+');">'+(data.total)+'</a></li>');
          			navUl.append('<li><a href="javascript:load('+(data.now+1)+');">下一页</a></li>');
				}
			});
		},2000);
	});
}
function getUrl(page){
    return "static/json/data-list-my-points.json";
}