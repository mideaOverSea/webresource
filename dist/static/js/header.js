$(document).ready(function(){

    /**全局变量*/
    _ACTIVE_ = "active";

    /**判断浏览器版本*/
    var $userAgent = navigator.userAgent.toLowerCase();
    $.browser = {
        version: ($userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        safari: /webkit/.test($userAgent),
        opera: /opera/.test($userAgent),
        msie: /msie/.test($userAgent) && !/opera/.test($userAgent),
        mozilla:/mozilla/.test($userAgent)&&!/(compatible|webkit)/.test($userAgent)
    };

    /**顶部导航轮播事件*/
    function headerNav(swiper){
        var $CName;
        $(".J-navLink3-item").each(function(e){
            var $_this = $(this);
            var $itemChild = $_this.find(".J-nav-container");
            $CName = "link3-con-"+e;
            nextBtn = "link3-next-"+e;
            prevBtn = "link3-prev-"+e;
            $itemChild.addClass($CName);
            $itemChild.parents(".navLink3-item-wrap").siblings(".link3-next").addClass(nextBtn);
            $itemChild.parents(".navLink3-item-wrap").siblings(".link3-prev").addClass(prevBtn);
            var $slide_len = $itemChild.find(".swiper-slide").length;
            var $last_slide_len = $(".link3-con-3").find(".swiper-slide").length;

            var $swiperNav = new Swiper('.'+$CName,{
                nextButton:'.link3-next-'+e,
                prevButton:'.link3-prev-'+e,
                slidesPerView : 8,
                calculateHeight : true,
                autoplay : false,
                noSwiping : true,
                noSwipingClass : 'noSwiping',
                loop:false,
                observer: true,
                observeParents: true,
            });
            $(".J-first-nav li").bind('mouseenter',function(event){
                $swiperNav.init();
            });

            if($slide_len > 8){
                $itemChild.siblings(".btn-next").show();
                $itemChild.siblings(".btn-prev").show();

            }else{
                $itemChild.siblings(".btn-next").hide();
                $itemChild.siblings(".btn-prev").hide();
            }
            if($.browser.msie&&($.browser.version === "9.0")){
                $(".J-first-nav li").bind('mouseenter',function(event){
                    $swiperNav.reInit();
                })

                $('.link3-prev-'+e).on('click', function(e){
                    e.preventDefault();
                    $swiperNav.swipePrev();
                })
                $('.link3-next-'+e).on('click', function(e){
                    e.preventDefault();
                    $swiperNav.swipeNext();
                })
            }
        })
    }
    headerNav();

    //导航鼠标移入
    $(".J-first-nav li").bind('mouseenter',function(event){
        $(".header-bgColor").fadeIn(200);
        $(this).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
    })


    $(".J-nav-link1 li:first-child").hover(function(){
        $(".J-nav-link3").addClass(_ACTIVE_);
    })
    $(".J-nav-link1 li:not(:first-child)").hover(function(){
        $(".J-nav-link3").removeClass(_ACTIVE_);
    })

    $(".J-nav-link3-ul li").hover(function(){
        $(this).parent(".J-nav-link3-ul").siblings().find(".J-navLink3-item").eq($(this).index()).addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
    })

    //导航鼠标移出
    $(".J-first-nav").bind('mouseleave',function(event){
        $(".J-first-nav li").removeClass(_ACTIVE_);
        $(".header-bgColor").fadeOut(200);
        setTimeout(function(){
            $(".J-nav-link2").show();
            $(".J-nav-link3").removeClass(_ACTIVE_);
            $(".J-nav-link3-child .J-navLink3-item:first-child").addClass(_ACTIVE_).siblings().removeClass(_ACTIVE_);
        },500)
        event.stopPropagation();
    });

    function mobileHeader(){
        $(".J-mob-menu-btn .J-menu-btn").click(function(){
            if(!$(this).hasClass(_ACTIVE_)){
                $(this).addClass(_ACTIVE_);
                $(".J-mob-menu-list").addClass(_ACTIVE_);
                $(".J-mobile-bg").addClass(_ACTIVE_);

                $(".J-mob-search .J-m-showBtn").removeClass(_ACTIVE_);
                $(".J-mob-search-wrap").removeClass(_ACTIVE_);
            }else{
                $(this).removeClass(_ACTIVE_);
                $(".J-mob-menu-list").removeClass(_ACTIVE_);
                $(".J-mobile-bg").removeClass(_ACTIVE_);
            }
        })
        $(".J-mob-menu-list .J-a-link").click(function(){
            if(!$(this).hasClass(_ACTIVE_)){
                $(this).addClass(_ACTIVE_);
                $(this).siblings(".J-mob-nav-child").slideDown(200);
                $(this).parent().siblings().find(".J-a-link").removeClass(_ACTIVE_);
                $(this).parent().siblings().find(".J-mob-nav-child").slideUp(200);
            }else{
                $(this).removeClass(_ACTIVE_);
                $(this).siblings(".J-mob-nav-child").slideUp(200);
            }
        })
        $(".J-mob-search .J-m-showBtn").click(function(){
            if(!$(this).hasClass(_ACTIVE_)){
                $(this).addClass(_ACTIVE_);
                $(".J-mob-search-wrap").addClass(_ACTIVE_);
                $(".J-mobile-bg").addClass(_ACTIVE_);

                $(".J-mob-menu-btn .J-menu-btn").removeClass(_ACTIVE_);
                $(".J-mob-menu-list").removeClass(_ACTIVE_);
            }else{
                $(this).removeClass(_ACTIVE_);
                $(".J-mob-search-wrap").removeClass(_ACTIVE_);
                $(".J-mobile-bg").removeClass(_ACTIVE_);
            }
        })
        $(".J-mob-search .J-m-closeBtn").click(function(){
            $(".J-mob-search .J-m-showBtn").removeClass(_ACTIVE_);
            $(".J-mob-search-wrap").removeClass(_ACTIVE_);
            $(".J-mobile-bg").removeClass(_ACTIVE_);
        })
        $(".J-mobile-bg").click(function(){
            $(this).removeClass(_ACTIVE_);
            $(".J-mob-search .J-m-showBtn").removeClass(_ACTIVE_);
            $(".J-mob-search-wrap").removeClass(_ACTIVE_);
            $(".J-mob-menu-btn .J-menu-btn").removeClass(_ACTIVE_);
            $(".J-mob-menu-list").removeClass(_ACTIVE_);
        })
    }
    mobileHeader();

    /*移动端导航-晒单有礼二维码弹框*/
    $(".J-progBtn").click(function(){
        $(".J-mob-menu-list").removeClass(_ACTIVE_);
        $(".J-mobile-bg").removeClass(_ACTIVE_);
        $(".J-mob-menu-btn .J-menu-btn").removeClass(_ACTIVE_);
        setTimeout(function(){
            $(".J-prog-popup").fadeIn(200);
        },200)
    });
    $(".J-prog-popup-con").click(function(){
        return false;
    });
    $(".J-prog-popup").click(function(){
        $(".J-prog-popup").fadeOut(200);
    });

    $(".J-search .J-sc-btn,.J-search-popup").click(function(){
        $(".J-search-popup").addClass(_ACTIVE_);
        event.stopPropagation();
    })
    $(".J-search-popup .J-sp-input").click(function(){
        $(this).siblings("dl").show();
    })
    $(document).click(function(event){
        $(".J-search-popup").removeClass(_ACTIVE_);
        $(".J-search-popup dl").hide();
        event.stopPropagation();
    })

})