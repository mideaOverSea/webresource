! function(v) {
    var n = {
        format: "yyyy-MM-dd",
        callBack: function() {},
        maxDate: new Date,
        minDate: "1900-01-01",
        defDate: "",
        defIsShow: !1,
        inputIsRead: !1
    };

    function r() {
        for (var e = "<ul class='hourHsDate'>", t = 1; t <= 24; t++) e += t <= 9 ? "<li class='hs_hour'>0" + t + "时</li>" : "<li class='hs_hour'>" + t + "时</li>";
        return e += "</ul>"
    }

    function l(e) {
        for (var t = "<ul class='dayHsDate'>", s = 1; s <= e; s++) t += "<li class='hs_day' >" + s + "</li>";
        return t += "</ul>"
    }

    function i() {
        for (var e = "<ul class='minuteHsDate'>", t = 0; t < 60; t++) e += t <= 9 ? "<li class='hs_minute'>0" + t + "分</li>" : "<li class='hs_minute'>" + t + "分</li>";
        return e += "</ul>"
    }

    function p(e) {
        var t = new Date(e);
        return t.getFullYear() + "-" + (t.getMonth() + 1) + "-" + t.getDate() + " " + t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds()
    }

    function H(e) {
        return e % 4 == 0 && e % 100 != 0 || e % 400 == 0
    }

    function C(e, t) {
        var s = new Date(e),
            a = function(e, t) {
                t || (t = 2), e = String(e);
                for (var s = 0, a = ""; s < t - e.length; s++) a += "0";
                return a + e
            };
        return t.replace(/"[^"]*"|'[^']*'|\b(?:d{1,4}|m{1,4}|yy(?:yy)?|([hHMstT])\1?|[lLZ])\b/g, function(e) {
            switch (e) {
                case "d":
                    return s.getDate();
                case "dd":
                    return a(s.getDate());
                case "ddd":
                    return ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"][s.getDay()];
                case "dddd":
                    return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][s.getDay()];
                case "M":
                    return s.getMonth() + 1;
                case "MM":
                    return a(s.getMonth() + 1);
                case "MMM":
                    return ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][s.getMonth()];
                case "MMMM":
                    return ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][s.getMonth()];
                case "yy":
                    return String(s.getFullYear()).substr(2);
                case "yyyy":
                    return s.getFullYear();
                case "h":
                    return s.getHours() % 12 || 12;
                case "hh":
                    return a(s.getHours() % 12 || 12);
                case "H":
                    return s.getHours();
                case "HH":
                    return a(s.getHours());
                case "m":
                    return s.getMinutes();
                case "mm":
                    return a(s.getMinutes());
                case "s":
                    return s.getSeconds();
                case "ss":
                    return a(s.getSeconds());
                case "l":
                    return a(s.getMilliseconds(), 3);
                case "L":
                    var t = s.getMilliseconds();
                    return 99 < t && (t = Math.round(t / 10)), a(t);
                case "tt":
                    return s.getHours() < 12 ? "am" : "pm";
                case "TT":
                    return s.getHours() < 12 ? "AM" : "PM";
                case "Z":
                    return s.toUTCString().match(/[A-Z]+$/);
                default:
                    return e.substr(1, e.length - 2)
            }
        })
    }

    function M(e) {
        v(e).remove()
    }
    v.fn.jHsDate = function(d) {
        d = v.extend({}, n, d || {});
        var o = this;
        d.inputIsRead && o.attr("readonly", "readonly"), o.attr("name", "jHsDateInput"), d.maxDate == d.minDate && alert("最大值最小值错误"), d.maxDate < d.defDate && "" != d.defDate && (d.defDate = d.maxDate), d.minDate > d.defDate && "" != d.defDate && (d.defDate = d.minDate);
        var u = "",
            e = "",
            t = "",
            s = "",
            a = "",
            c = "",
            f = o.attr("id");
        if (null != f) {
            var D = v("#date").width();
            v(window).resize(function() {
                v("#date").width()
            }), u = "<div id='" + f + "hsdate' name='jHsDate' style='box-shadow: 0 3px 5px rgba(221,221,221,0.5);'>";
            var h = d.format,
                m = p(d.maxDate),
                k = p(d.minDate),
                _ = ""; - 1 < h.indexOf("y") && (e = function(e, t) {
                for (var s = "<ul class='yearHsDate'>", a = e; t <= a; a--) s += "<li class='hs_year' >" + a + "</li>";
                return s += "</ul>"
            }(m.split("-")[0], k.split("-")[0]), _ += "y"), -1 < h.indexOf("M") && (t = function() {
                for (var e = "<ul class='monHsDate'>", t = 1; t <= 12; t++) e += "<li class='hs_mon'>" + t + "月</li>";
                return e += "</ul>"
            }(), _ += "M"), -1 < h.indexOf("d") && (s = l(31), _ += "d"), u += e + t + s + a + c, u += "</div>", o.click(function() {
                if (0 < v("#" + f + "hsdate").length) return !1;
                var e, t, s, a, c, h;
                v("body").append(u), e = d.defDate, t = f + "hsdate", s = new Date(e), a = s.getFullYear(), c = s.getMonth() + 1 + "月", h = s.getDate(), v("#" + t).children(".yearHsDate").children(".hs_year").each(function() {
                    if (v(this).text() == a) return v(this).addClass("hs_check"), v(this).parent().scrollTop(v(this).position().top), !1
                }), v("#" + t).children(".monHsDate").children(".hs_mon").each(function() {
                    if (v(this).text() == c) return v(this).addClass("hs_check"), -1 < _.indexOf("d") && g(c), y(_), !1
                }), v("#" + t).children(".dayHsDate").children(".hs_day").each(function() {
                    if (v(this).text() == h) return v(this).addClass("hs_check"), v(this).parent().scrollTop(v(this).position().top), !1
                }), -1 < d.format.indexOf("h") && y(_ + "h");
                var n = o.offset().top,
                    r = o.offset().left,
                    l = o.outerHeight(!1),
                    i = "";
                v("#" + f + "hsdate").css({
                    top: n + (l + 5),
                    left: r,
                    width: D
                }), v(".hs_year").click(function() {
                    v(".yearHsDate> .hs_check").removeClass("hs_check"), v(this).addClass("hs_check"), v(".monHsDate> .noCheck").removeClass("noCheck"), (i = v(this).text()) == new Date(m).getFullYear() && (v(".monHsDate> .hs_check").removeClass("hs_check"), v("#" + f + "hsdate").children(".monHsDate").children(":gt(" + new Date(m).getMonth() + ")").addClass("noCheck")), i == new Date(k).getFullYear() && (v(".monHsDate> .hs_check").removeClass("hs_check"), v("#" + f + "hsdate").children(".monHsDate").children(":lt(" + new Date(k).getMonth() + ")").addClass("noCheck"))
                }), v(".hs_mon").click(function() {
                    v(this).hasClass("noCheck") || (v(".monHsDate> .hs_check").removeClass("hs_check"), v(this).addClass("hs_check"), -1 < _.indexOf("d") && g(v(this).text()), v(".dayHsDate> .noCheck").removeClass("noCheck"), i == new Date(m).getFullYear() && v(this).text() == new Date(m).getMonth() + 1 + "月" && (v(".dayHsDate> .hs_check").removeClass("hs_check"), v("#" + f + "hsdate").children(".dayHsDate").children(":gt(" + (new Date(m).getDate() - 1) + ")").addClass("noCheck")), i == new Date(k).getFullYear() && v(this).text() == new Date(m).getMonth() + 1 + "月" && (v(".dayHsDate> .hs_check").removeClass("hs_check"), v("#" + f + "hsdate").children(".dayHsDate").children(":lt(" + (new Date(k).getDate() - 1) + ")").addClass("noCheck"), v('[class="hs_day"]').parent().scrollTop(v('[class="hs_day"]').position().top)), y(_))
                })
            }), v(document).click(function(e) {
                e = window.event || e, obj = v(e.srcElement || e.target), v(obj).is("[name='jHsDateInput']") || v(obj).is('[class^="hs_"]') || v('[name="jHsDate"]').remove()
            }), d.defIsShow && "" != d.defDate && o.val(C(d.defDate, d.format))
        } else alert("元素Id或元素不存在");

        function y(e) {
            switch (e) {
                case "y":
                    v(".hs_year").click(function() {
                        v(o).val(v('[class="hs_year hs_check"]').text()), M(v("#" + f + "hsdate")), d.callBack()
                    });
                    break;
                case "yM":
                    v(".hs_mon").click(function() {
                        var e = v('[class="hs_year hs_check"]').text() + "-" + v('[class="hs_mon hs_check"]').text();
                        v(o).val(C(e.replace("月", ""), d.format)), M(v("#" + f + "hsdate")), d.callBack()
                    });
                    break;
                case "yMd":
                    v(".hs_day").click(function() {
                        v(".dayHsDate>.hs_check").removeClass("hs_check"), v(this).addClass("hs_check");
                        var e = v('[class="hs_year hs_check"]').text() + "-" + v('[class="hs_mon hs_check"]').text() + "-" + v('[class="hs_day hs_check"]').text(); - 1 < h.indexOf("h") ? (a = r(), c = i(), v("#" + f + "hsdate").html(a + c), v("#" + f + "hsdate").children(".hourHsDate").children().click(function() {
                            e = e.split(" ")[0], e += " " + v(this).text().replace("时", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check")
                        }), v("#" + f + "hsdate").children(".minuteHsDate").children().click(function() {
                            e = e.split(":")[0], e += ":" + v(this).text().replace("分", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check"), v(o).val(C(e.replace("月", ""), d.format)), M(v("#" + f + "hsdate")), d.callBack()
                        })) : (v(o).val(C(e.replace("月", ""), d.format)), M(v("#" + f + "hsdate")), d.callBack())
                        $("#date").siblings(".tips").text("").addClass("success");
                    });
                    break;
                case "h":
                    var t = "";
                    a = r(), c = i(), v("#" + f + "hsdate").html(a + c), v("#" + f + "hsdate").children(".hourHsDate").children().click(function() {
                        t = "", t = v(this).text().replace("时", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check")
                    }), v("#" + f + "hsdate").children(".minuteHsDate").children().click(function() {
                        t = t.split(":")[0], t += ":" + v(this).text().replace("分", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check"), v(o).val(t), M(v("#" + f + "hsdate"))
                    });
                    break;
                default:
                    v(".hs_day").click(function() {
                        v(".dayHsDate>.hs_check").removeClass("hs_check"), v(this).addClass("hs_check");
                        var e = v('[class="hs_mon hs_check"]').text() + "-" + v('[class="hs_day hs_check"]').text(); - 1 < h.indexOf("h") ? (a = r(), c = i(), v("#" + f + "hsdate").html(a + c), v("#" + f + "hsdate").children(".hourHsDate").children().click(function() {
                            e += " " + v(this).text().replace("时", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check")
                        }), v("#" + f + "hsdate").children(".minuteHsDate").children().click(function() {
                            e = e.split(":")[0], e += ":" + v(this).text().replace("分", ""), v(this).parent().children().removeClass("hs_check"), v(this).addClass("hs_check"), v(o).val(C(e.replace("月", ""), d.format)), M(v("#" + f + "hsdate"))
                        })) : (v(o).val(C(e.replace("月", ""), d.format)), M(v("#" + f + "hsdate")))

                    })
            }
        }

        function g(e) {
            H(v(".yearHsDate>.hs_check").text()) && "2月" == e ? s = l(29) : "1月" == e || "3月" == e || "5月" == e || "7月" == e || "8月" == e || "10月" == e || "12月" == e ? s = l(31) : "4月" == e || "6月" == e || "9月" == e || "11月" == e ? s = l(30) : H(v(".yearHsDate>.hs_check").text()) || "2月" != e || (s = l(28)), v(".dayHsDate").remove(), v(".monHsDate").after(s)
        }
    }
}(jQuery);